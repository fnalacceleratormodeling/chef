module load cmake
# boost/1.6[123] give error in linking because of undefined
# symbol in  boost::serialization::array_wrapper<int>
# seen by other people
#https://stackoverflow.com/questions/39497971/error-linking-to-boost-serialization
#https://svn.boost.org/trac10/ticket/12516

module switch craype-haswell craype-mic-knl

# The boost module must supply boost.python, suitable for use with
# python 3.x. This is the first boost version for which such a module
# is available on cori.
module load boost/1.72.0

module load cray-fftw
# the FFTW module definition doesn't add the library location into
# LD_LIBRARY_PATH
LD_LIBRARY_PATH=$FFTW_DIR:$LD_LIBRARY_PATH
module load python
module load gsl
module load cray-hdf5
module unload craype-hugepages2M # this avoids crashes in Python

export CMAKE_PREFIX_PATH=${FFTW_ROOT}:${CMAKE_PREFIX_PATH}
export CRAYPE_LINK_TYPE=dynamic
