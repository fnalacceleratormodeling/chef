/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******
******  File:      InducedKickPropagators.h
******                                                                
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Author:    Eric Stern    egstern@fnal.gov
******                                                                
******                                                                
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Jan 2016           egstern@fnal.gov
******  - initial version.
******  - class declaration for propagators to be used by
******    class InducedKick.
******                                                                
**************************************************************************
*************************************************************************/

#ifndef INDUCEDKICKPROPAGATORS_H
#define INDUCEDKICKPROPAGATORS_H


class Particle;
class JetParticle;

class InducedKick::Propagator: public BasePropagator<InducedKick> {

 public:

  Propagator* Clone() const { return new Propagator(*this); }
 
  void  setup( InducedKick& elm ); 

  void  operator()(  InducedKick& elm,             Particle& p);
  void  operator()(  InducedKick& elm,          JetParticle& p);

};


#endif    // INDUCEDKICKPROPAGATORS_H
