/*************************************************************************
**************************************************************************
**************************************************************************
******
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and
******             synchrotrons.
******
******  File:      McMLens.h
******
******  Copyright (c) Fermi Research Alliance
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******
******  Author:    Eric G. Stern
******             Email: egstern@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******
******  Nov 2020            egstern@fnal.gov
******  - implement McMillan lens
******
**************************************************************************
*************************************************************************/

#ifndef MCMLENS_H
#define MCMLENS_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>


class BmlVisitor;
class ConstBmlVisitor;
class McMLens;

typedef boost::shared_ptr<McMLens> McMLensPtr;
typedef boost::shared_ptr<McMLens const> ConstMcMLensPtr;


class DLLEXPORT McMLens : public bmlnElmnt {

  class Propagator;  

public:

  typedef boost::shared_ptr<BasePropagator<McMLens> > PropagatorPtr; 


  // Ctors
  // -----
  McMLens( char const* name, double l, double j0, double beta_e, double rm );
  McMLens( McMLens const& );

  McMLens* Clone() const;

  McMLens& operator=( McMLens const& rhs );

  ~McMLens();

  // Modifiers
  // ---------
  void set_j0( double x );
  void set_beta_e( double x);
  void set_rm( double x );

  double get_j0() { return strength_; }
  double get_beta_e() { return beta_e; }
  double get_rm() { return rm; }

  // Propagators
  // -----------
  void localPropagate(         Particle& p );
  void localPropagate(      JetParticle& p );
  void localPropagate(    ParticleBunch& b ); 
  void localPropagate( JetParticleBunch& b ); 

  // Visitor Methods
  // ---------------
  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  // Queries
  // -------
  const char* Type() const;
  bool isMagnet() const;

  void usePropagator( PropagatorPtr& );

private:

  McMLens();         // default constructor forbidden

  double beta_e;
  double rm;

  PropagatorPtr            propagator_;

  std::ostream& writeTo(std::ostream&);
  std::istream& readFrom(std::istream&);

} ;

#endif // MCMLENS_H
