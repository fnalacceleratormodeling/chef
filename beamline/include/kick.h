/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      kick.h
******                                                                
******  Copyright Universities Research Association, Inc./ Fermilab    
******            All Rights Reserved                             
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
******                                                                
******  Author:    Leo Michelotti                                     
******                                                                
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 220                                      
******             Batavia, IL   60510                                
******                                                                
******             Phone: (630) 840 4956                              
******             Email: michelotti@fnal.gov                         
******                                                                
******                                                                
****** REVISION HISTORY
******
****** Mar 2007:          ostiguy@fnal.gov
****** - use covariant return types
****** - support for reference counted elements
****** 
****** Dec 2007:          ostiguy@fnal.gov
****** - new typesafe propagators
****** 
****** Jan 2008:          ostiguy@fnal.gov
****** - kick now assumed to scale with momentum. the strength_
******   data member is now the bend strength, _not_ the bend angle
****** - change to implementation. The ratio between h
******   and v strength is now stored in the general
******   kick element so that RefRegVisitor can perform
******   the scaling properly.
****** 
****** Mar 2013:          michelotti@fnal.gov
****** - fixed an error discovered by Eric Stern:
******   added member functions [h|v|]kick::Split(...).
******   Previous usage of the default bmlnElmnt::Split(...)
******   did not take into account that, unlike other elements,
******   the strength_ attribute of a kick is (now) field x length,
******   not just field. (See preceding note: Jan 2008.)
****** 
**************************************************************************
*************************************************************************/

#ifndef KICK_H
#define KICK_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>

class BmlVisitor;
class ConstBmlVisitor;


class hkick;

typedef boost::shared_ptr<hkick>        HKickPtr;
typedef boost::shared_ptr<hkick const>  ConstHKickPtr;

class vkick;
typedef boost::shared_ptr<vkick>        VKickPtr;
typedef boost::shared_ptr<vkick const>  ConstVKickPtr;

class kick;
typedef boost::shared_ptr<kick>        KickPtr;
typedef boost::shared_ptr<kick const>  ConstKickPtr;

class DLLEXPORT hkick : public bmlnElmnt {

  class Propagator;

public:


  typedef boost::shared_ptr<BasePropagator<hkick> > PropagatorPtr;   

  hkick();
  hkick( char const* name);                                         // name; assumes zero kick
  hkick( char const* name,                 double kick);     // kick size in radians
  hkick( char const* name,  double length, double kick);     // kick size in radians
  hkick( hkick const& );

  hkick* Clone() const { return new hkick( *this ); }

  hkick& operator=( hkick const& rhs);

 ~hkick();

  void localPropagate(         Particle& );
  void localPropagate(      JetParticle& );
  void localPropagate(    ParticleBunch& );
  void localPropagate( JetParticleBunch& );

  const char* Type()       const;
  bool        isMagnet()   const;

  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  void Split( double, ElmPtr&, ElmPtr& ) const;

  void usePropagator( PropagatorPtr& );

 private:
 
  PropagatorPtr propagator_;

};



class DLLEXPORT vkick : public bmlnElmnt {

  class Propagator;

public:

  typedef boost::shared_ptr<BasePropagator<vkick> > PropagatorPtr;   

  vkick();                                                             // Assumes zero kick
  vkick( char const* );                                                // name; assumes zero kick
  vkick( char const*  name, double kick);                       // kick size in radians
  vkick( char const*,double length, double kick );       // kick size in radians
  vkick( vkick const& );

  vkick* Clone() const { return new vkick( *this ); }

  vkick& operator=( vkick const& rhs);

 ~vkick();

  void localPropagate(         Particle& );
  void localPropagate(      JetParticle& );
  void localPropagate(    ParticleBunch& );
  void localPropagate( JetParticleBunch& );

  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  const char* Type()       const;
  bool        isMagnet()   const;

  void Split( double, ElmPtr&, ElmPtr& ) const;

  void usePropagator( PropagatorPtr& );

 private:

  PropagatorPtr propagator_;

};


class DLLEXPORT kick : public bmlnElmnt {

  class Propagator;

public:

  typedef boost::shared_ptr<BasePropagator<kick> > PropagatorPtr;   

  kick();
  kick( char const* name );
  kick( char const* name,                         double horizontal_kick, double vertical_kick);
  kick( char const* name,   double length, double horizontal_kick, double vertical_kick);

  kick( kick const& );

  kick* Clone() const { return new kick( *this ); }

  kick& operator=( kick const& rhs);

 ~kick();

  void localPropagate(         Particle& );
  void localPropagate(      JetParticle& );
  void localPropagate(    ParticleBunch& );
  void localPropagate( JetParticleBunch& );


  void accept(BmlVisitor& v);
  void accept(ConstBmlVisitor& v) const;

  double getHorStrength() const;
  double        getVerStrength() const;

  void setHorStrength( double value);
  void setVerStrength( double value);

  const char* Type()       const;
  bool        isMagnet()   const;

  void Split( double, ElmPtr&, ElmPtr& ) const;

  void usePropagator( PropagatorPtr& );

private:

  double vh_ratio_;

  PropagatorPtr propagator_;

  std::istream& readFrom(std::istream&);
  std::ostream& writeTo(std::ostream&);

 };

#endif    // KICK_H
