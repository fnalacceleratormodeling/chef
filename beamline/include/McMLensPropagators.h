/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      McMLensPropagtors.h
******                                                                
******  Copyright Fermi Research Alliance / Fermilab    
******            All Rights Reserved                             
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-07CH11359 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws.
******                                                                
******                                                                
******
******  Author:    Eric G. Stern
******             Email: egstern@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******
******  Aug 2014            cspark@fnal.gov
******  - implement non-linear lens first time
******
**************************************************************************
*************************************************************************/

#ifndef  MCMLENSPROPAGATORS_H
#define  MCMLENSPROPAGATORS_H

#include <beamline/McMLens.h>

class Particle;
class JetParticle;

class McMLens::Propagator: public BasePropagator<McMLens> {

 public:

  Propagator* Clone() const { return new Propagator(*this); }

  void setup( McMLens& elm );

  void  operator()( McMLens& elm,             Particle& p);
  void  operator()( McMLens& elm,          JetParticle& p);

};

#endif //  MCMLENSPROPAGATORS_H
