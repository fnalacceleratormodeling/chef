/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      Edge.h
******                                                                
******  Copyright (c) Fermi Research Alliance
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Authors:   Leo Michelotti         michelotti@fnal.gov
******             Jean-Francois Ostiguy  ostiguy@fnal.gov
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Dec 2007         ostiguy@fnal.gov
******  - original version
******
******  Aug 2014         michelotti@fnal.gov
******  - allowing user-defined, "plug-n-play" propagators 
******    via XXXXX.usePropagator(...)
******  
******  Aug 2016         michelotti@fnal.gov
******  - defining token NO_FIXED_ENTRY_ANGLE as true;
******    see comment lines below.
******
**************************************************************************
*************************************************************************/

#ifndef EDGE_H
#define EDGE_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>

#define NO_FIXED_ENTRY_ANGLE false

// The default operation of bending magnets is no being set use
// fixed, "hardwired" entry angles in determining
// edge focusing.  The other option of calculating the entry angle dynamically from the
// incoming particle itself is not symplectic. This is controlled with by setting the value of
// the NO_FIXED_ENTRY_ANGLE directive. Setting the value to false uses fixed entry entry
// and exit angles.  Setting it to true calculates the angles particle-by-particle.
//
// The flag can be set by defining it in this include file, or by removing the above definition
// and setting the value in a -DNO_FIXED_ENTRY_ANGLE={true|false} compilation flag.
// 
// Using #define in a header file is generally not a good idea, but the
// choice is between putting the line here, in one place - in a file
// included by all bend propagator source codes - or separately in
// multiple files. The former seems preferable to the latter.
// 
// - Leo Michelotti
//   August, 2016

class BmlVisitor;
class ConstBmlVisitor;
class quadrupole;
class Edge;

typedef boost::shared_ptr<Edge>              EdgePtr; 
typedef boost::shared_ptr<Edge const>   ConstEdgePtr; 

class DLLEXPORT Edge : public bmlnElmnt {

  class Propagator;  

public:

  typedef boost::shared_ptr<BasePropagator<Edge> > PropagatorPtr;   

  Edge();
  Edge( char const* name, double strength );    
  Edge( Edge const& );

  Edge* Clone() const;

 ~Edge();

  void localPropagate(           Particle& p );
  void localPropagate(        JetParticle& p );
  void localPropagate(      ParticleBunch& b );
  void localPropagate(   JetParticleBunch& b );

  void accept( BmlVisitor& v );           
  void accept( ConstBmlVisitor& v ) const;

  bool        isMagnet()   const;
  char const* Type()       const;

  void usePropagator( PropagatorPtr& );

 private:

  PropagatorPtr     propagator_;  

};

#endif // EDGE_H
