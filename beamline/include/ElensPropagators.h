/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******
******  File:      ElensPropagators.h
******                                                                
******  Copyright (c) Fermi Research Alliance LLC 
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-07CH11359 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
******
******                                                                
******  Author:    Eric G. Stern
******             egstern@fnal.gov
****** 
******                                                                
**************************************************************************
*************************************************************************/

#ifndef ELENSPROPAGATORS_H
#define ELENSPROPAGATORS_H


class Particle;
class JetParticle;

class elens::Propagator: public BasePropagator<elens> {

 public:

  Propagator* Clone() const { return new Propagator(*this); }
 
  void  setup( elens& elm );

  void  operator()(  elens& elm,             Particle& p);
  void  operator()(  elens& elm,          JetParticle& p);

};

#endif    // ELENSPROPAGATORS_H
