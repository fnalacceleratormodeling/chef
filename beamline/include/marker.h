/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      marker.h
******                                                                
******  Copyright (c) 1991 Universities Research Association, Inc.    
******                All Rights Reserved                             
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
****** 
******                                                                
******  Author:    Leo Michelotti                                     
******                                                                
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 220                                      
******             Batavia, IL   60510                                
******                                                                
******             Phone: (630) 840 4956                              
******             Email: michelotti@fnal.gov                         
******                                                                
******                                                                
****** REVISION HISTORY
******
****** Mar 2007            ostiguy@fnal.gov
****** - use covariant return types
****** - support for reference counted elements
****** Dec 2007            ostiguy@fnal.gov
****** - new typesafe propagators
******
**************************************************************************
*************************************************************************/
#ifndef MARKER_H
#define MARKER_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>

class BmlVisitor;
class ConstBmlVisitor;

class marker;

typedef boost::shared_ptr<marker>        MarkerPtr;
typedef boost::shared_ptr<marker const>  ConstMarkerPtr;


class DLLEXPORT marker : public bmlnElmnt {

  class Propagator;

public:

  typedef boost::shared_ptr<BasePropagator<marker> > PropagatorPtr;   

  marker();                       // Data to be written to standard output
  marker( char const* name);      // Name identifier.
  marker( marker const& );

  marker* Clone() const { return new marker( *this ); }

  marker& operator=( marker const& rhs);

  virtual ~marker();

  void localPropagate(        Particle&   );
  void localPropagate(     JetParticle&   );
  void localPropagate(   ParticleBunch&   );
  void localPropagate( JetParticleBunch&  );

  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  const char* Type()     const;
  bool        isMagnet() const;

  void usePropagator( PropagatorPtr& );

 private:

  PropagatorPtr propagator_; 
};

#endif // MARKER_H
