/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      septum.h
******                                                                
******  Copyright (c) 1991 Universities Research Association, Inc.    
******                All Rights Reserved                             
******                                                                
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
****** 
******  Author:    Leo Michelotti                                     
******                                                                
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 220                                      
******             Batavia, IL   60510                                
******                                                                
******             Phone: (630) 840 4956                              
******             Email: michelotti@fnal.gov                         
******                                                                
******                                                                
******  REVISION HISTORY
******  Mar 2007           ostiguy@fnal.gov
****** - use covariant return types
****** - support for reference counted elements
******
******  Dec 2007           ostiguy@fnal.gov
****** - new typesafe propagator scheme
******
******  Sep 2012           cspark@fnal.gov
****** - new class septum added for finite length
******
**************************************************************************
*************************************************************************/

#ifndef SEPTUM_H
#define SEPTUM_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>

class BmlVisitor;
class ConstBmlVisitor;

class septum;
class thinSeptum;

typedef boost::shared_ptr<septum>                     SeptumPtr;
typedef boost::shared_ptr<septum const>          ConstSeptumPtr;
typedef boost::shared_ptr<thinSeptum>             ThinSeptumPtr;
typedef boost::shared_ptr<thinSeptum const>  ConstThinSeptumPtr;

class DLLEXPORT septum : public bmlnElmnt
{

  class Propagator;

public:

  typedef boost::shared_ptr<BasePropagator<septum> > PropagatorPtr;

  septum( char const* name, double length );

  septum( char const* name, double length, double voltage, double g );

  septum( char const* name, double length, double voltage, double g, double xw );

  septum( char const* name, double length, double voltage, double g, double xw, double ww );

  septum( septum const& );

  septum* Clone() const { return new septum( *this ); }

  septum& operator=( septum const& rhs);

 ~septum();

  void setVoltage( double x );
  void setGap( double x );
  void setWire( double x);
  void setWireWidth( double x );
  void setReportNumber( int const& );

  double getVoltage()     { return voltage_;     }
  double getGap()         { return gap_;         }
  double getWireX()       { return xWire_;       }
  double getWireWidth()   { return wireWidth_;   }

  void localPropagate(         Particle&  p );
  void localPropagate(      JetParticle&  p );
  void localPropagate(    ParticleBunch&  p );
  void localPropagate( JetParticleBunch&  p );

  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  bool    isMagnet() const;
  const char* Type() const;

  int numberKicked_;      // ??? TODO: SHOULD BE PRIVATE ???
  int numberBadHits_;     // ??? TODO: SHOULD BE PRIVATE ???
  int numberBackHits_;    // ??? TODO: SHOULD BE PRIVATE ???
  int numberOutGap_;      // ??? TODO: SHOULD BE PRIVATE ???
  int turnNumber_;        // ??? TODO: SHOULD BE PRIVATE ???
  int reportNumber_;      // ??? TODO: SHOULD BE PRIVATE ???

  void usePropagator( PropagatorPtr& );

private:

  septum();               // default constructor forbidden

  double voltage_;        // voltage of septum wire/foil in kV
  double gap_;            // gap between wire and anode in meters
  double xWire_;          // position of wire septum in meters
  double wireWidth_;      // width of wire in meters

  PropagatorPtr  propagator_;

  std::ostream& writeTo(std::ostream&);
  std::istream& readFrom(std::istream&);

};

class DLLEXPORT thinSeptum : public bmlnElmnt
{

  class Propagator; 

public:

  typedef boost::shared_ptr<BasePropagator<thinSeptum> > PropagatorPtr;   

  thinSeptum( char const*  name );
  
  thinSeptum( char const*  name,
	    double sPos,    // kick in strength in radians for x > xWire
	    double sNeg,    // kick in strength in radians for x < xWire
	    double x );     // position of wire septum in meters
   
  thinSeptum( double sPos,
	    double sNeg,
	    double x );
  
  thinSeptum( thinSeptum const& );

  thinSeptum* Clone() const { return new thinSeptum( *this ); }

  thinSeptum& operator=( thinSeptum const& rhs);

 ~thinSeptum();
  
  void setStrengths( double sPos, double sNeg);
  void setWire( double x);
  void setWireWidth( double x );
  void setGap( double x );
  void setReportNumber( int const& );

  double getPosStrength() { return strengthPos_; }
  double getNegStrength() { return strengthNeg_; }
  double getWireX()       { return xWire_;       }
  double getWireWidth()   { return wireWidth_;   }
  double getGap()         { return gap_;         }
  
  void localPropagate(         Particle&  p );
  void localPropagate(      JetParticle&  p );
  void localPropagate(    ParticleBunch&  p );
  void localPropagate( JetParticleBunch&  p );

  void accept( BmlVisitor& v ); 
  void accept( ConstBmlVisitor& v ) const; 

  bool    isMagnet() const;
  const char* Type() const;

  int numberKicked_;      // ??? TODO: SHOULD BE PRIVATE ???
  int numberBadHits_;     // ??? TODO: SHOULD BE PRIVATE ???
  int numberBackHits_;    // ??? TODO: SHOULD BE PRIVATE ???
  int numberOutGap_;      // ??? TODO: SHOULD BE PRIVATE ???
  int turnNumber_;        // ??? TODO: SHOULD BE PRIVATE ???
  int reportNumber_;      // ??? TODO: SHOULD BE PRIVATE ???

  void usePropagator( PropagatorPtr& );

private:
 
  thinSeptum();           // default constructor forbidden

  double strengthPos_;    // kick in strength in radians for x > xWire
  double strengthNeg_;	  // kick in strength in radians for x < xWire
  double xWire_;	  // position of wire septum in meters
  double wireWidth_;      // width of wire in meters
  double gap_;            // gap between wire and anode in meters

  PropagatorPtr  propagator_; 
  
  std::ostream& writeTo(std::ostream&);
  std::istream& readFrom(std::istream&);

};
 
#endif // SEPTUM_H
