/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      elens.h
******                                                                
******  Copyright Universities Research Association, Inc./ Fermilab    
******            All Rights Reserved                             
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
******                                                                
******  Author:    Eric G. Stern
******                                                                
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 103
******             Batavia, IL   60510                                
******                                                                
******             Phone: (630) 840 4747
******             Email: egstern@fnal.gov
******                                                                
******                                                                
****** REVISION HISTORY
******
****** 2018-01         Eric Stern
****** - initial version
****** 
**************************************************************************
*************************************************************************/

#ifndef ELENS_H
#define ELENS_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>

class BmlVisitor;
class ConstBmlVisitor;

class elens;

typedef boost::shared_ptr<elens>        ElensPtr;
typedef boost::shared_ptr<elens const>  ConstElensPtr;

class DLLEXPORT elens : public bmlnElmnt {

    class Propagator;

public:

  enum e_profile_t {
      undefined = 0,
      gaussian = 1,
      uniform = 2
  };

  typedef boost::shared_ptr<BasePropagator<elens> > PropagatorPtr;

  class        elens_core_access;
  friend class elens_core_access;

  // The attributes of the electron lens element are
  //   name
  //   length
  //   current
  //   electron velocity (energy)
  //   radius
  //   longitudinal rms (<0.0 indicates uniform)
  //   profiletype: [gaussian|uniform]
  //
  // For CHEF, the current becomes the strength attribute of beamline element

//  elens( char const* name);  // why would we even want this signature?
  elens( char const* name, double length, double current,
         double eenergy, double radius, double long_rms, e_profile_t const& prof);
  elens( elens const& );

  elens* Clone() const { return new elens( *this ); }

  elens& operator=( elens const& rhs);

 ~elens();

  void localPropagate(         Particle& );
  void localPropagate(      JetParticle& );
  void localPropagate(    ParticleBunch& );
  void localPropagate( JetParticleBunch& );

  const char* Type()       const;
  bool        isMagnet()   const;

  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  void Split( double, ElmPtr&, ElmPtr& ) const;

  void usePropagator( PropagatorPtr& );

 private:
  double eenergy;
  double radius;
  double longrms;
  e_profile_t profile;
  PropagatorPtr propagator_;

};

class elens::elens_core_access {

 public:

  static double&                    get_eenergy( elens& o)     { return o.eenergy;     }
  static double&                    get_radius( elens& o)      { return o.radius;     }
  static double&                    get_longrms( elens& o)     { return o.longrms;    }
  static e_profile_t&               get_profile( elens& o )    { return o.profile; }

};

#endif    // ELENS_H
