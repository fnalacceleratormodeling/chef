/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      InducedSextupoleKick.h
******                                                                
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Author:    Eric Stern    egstern@fnal.gov
******             Qiming Lu     qlu@fnal.gov
******                                                                
******                                                                
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Sep 2017           qlu@fnal.gov
******  - initial version.
******  - declaration for class InducedSextupoleKick, a magnetic kick
******    to be used in CF_sbend magnets.
******  - uses expressions for induced, effective magnetic fields 
******    for cylindrical geometry, derived independently
******    by Timofey Zolkin (2015) and Edwin McMillan (1975).
******  - implemented based on the InducedSextupoleKick class
****** 
**************************************************************************
*************************************************************************/

#ifndef INDUCEDSEXTUPOLEKICK_H
#define INDUCEDSEXTUPOLEKICK_H

#include <basic_toolkit/globaldefs.h>
#include <beamline/bmlnElmnt.h>

class BmlVisitor;
class ConstBmlVisitor;


class InducedSextupoleKick;

typedef boost::shared_ptr<InducedSextupoleKick>        InducedSextupoleKickPtr;
typedef boost::shared_ptr<InducedSextupoleKick const>  ConstInducedSextupoleKickPtr;


class DLLEXPORT InducedSextupoleKick : public bmlnElmnt {

  class Propagator;

public:


  typedef boost::shared_ptr<BasePropagator<InducedSextupoleKick> > PropagatorPtr;

  InducedSextupoleKick();
  InducedSextupoleKick( char const* name);                                    // name; assumes zero kick
  InducedSextupoleKick( char const* name, double k, double R0); // integrated quad strength k, radius R0
  InducedSextupoleKick( InducedSextupoleKick const& );

  InducedSextupoleKick* Clone() const { return new InducedSextupoleKick( *this ); }

  InducedSextupoleKick& operator=( InducedSextupoleKick const& rhs);

 ~InducedSextupoleKick();

  void localPropagate(         Particle& );
  void localPropagate(      JetParticle& );
  void localPropagate(    ParticleBunch& );
  void localPropagate( JetParticleBunch& );

  const char* Type()       const;
  bool        isMagnet()   const;

  void accept( BmlVisitor& v );
  void accept( ConstBmlVisitor& v ) const;

  void Split( double, ElmPtr&, ElmPtr& ) const;

  void usePropagator( PropagatorPtr& );

  inline double get_R0() const {return R0;} ;

 private:
  double R0;
  PropagatorPtr propagator_;

};


#endif    // INDUCEDSEXTUPOLEKICK_H
