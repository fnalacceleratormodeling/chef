/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******
******  File:      InducedSextupoleKickPropagators.h
******                                                                
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Author:    Eric Stern    egstern@fnal.gov
******             Qiming Lu     qlu@fnal.gov
******                                                                
******                                                                
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Sep 2017           qlu@fnal.gov
******  - initial version.
******  - class declaration for propagators to be used by
******    class InducedSextupoleKick.
******  - implemented based on the InducedKickPropagator
******                                                                
**************************************************************************
*************************************************************************/

#ifndef INDUCEDSEXTUPOLEKICKPROPAGATORS_H
#define INDUCEDSEXTUPOLEKICKPROPAGATORS_H


class Particle;
class JetParticle;

class InducedSextupoleKick::Propagator: public BasePropagator<InducedSextupoleKick> {

 public:

  Propagator* Clone() const { return new Propagator(*this); }
 
  void  setup( InducedSextupoleKick& elm ); 

  void  operator()(  InducedSextupoleKick& elm,             Particle& p);
  void  operator()(  InducedSextupoleKick& elm,          JetParticle& p);

};


#endif    // INDUCEDKICKPROPAGATORS_H
