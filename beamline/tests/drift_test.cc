#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>

#include <beamline/beamline.h>
#include <beamline/Particle.h>
#include <beamline/drift.h>
#include <beamline/JetParticle.h>
#include <mxyzptlk/TJetVector.h>

TEST_CASE("Drift", "[Drift]")
{
    const double pc = 2.784435311;
    const double drift_length = 1.0;

    // can I construct a drift?
    DriftPtr my_drift(new drift("foo", drift_length));
    REQUIRE(my_drift);

    // can I construct a beamline?
    BmlPtr my_beamline(new beamline("bar"));
    REQUIRE(my_beamline);

    // can I append my drift to the beamline and does it
    // get the element count right?
    my_beamline->append(my_drift);
    REQUIRE( my_beamline->countHowMany() == 1 );
    
    Proton probe;
    probe.SetReferenceMomentum(pc);
    double probe_beta = probe.ReferenceBeta();

    SECTION("propagating 0 particle") {
        my_beamline->propagate(probe);
        CHECK(probe.get_x() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_npx() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_y() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_npy() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_ndp() == Approx(0.0).margin(1.0e-14));

        // check time coordinate c*time is L/beta
        CHECK(probe.get_cdt() == Approx(drift_length/probe_beta));
    }

    SECTION("propagate crosswise across the drift") {
        const double x_offset = 0.1;
        // calculate angle and path length for diagonal crossing from
        // -x_offset to +x_offset over the drift length
        const double proton_path_length = 
            std::sqrt(drift_length*drift_length + 4.0*x_offset*x_offset);
        const double xprime = 2.0*x_offset/proton_path_length;

        probe.set_x(-x_offset);
        probe.set_npx(xprime);
        my_beamline->propagate(probe);

        CHECK( probe.get_x() == Approx(x_offset) );
        CHECK( probe.get_y() == Approx(0.0).margin(1.0e-15) );
        CHECK( probe.get_cdt() == Approx(proton_path_length/probe_beta) );
    }

    SECTION("propagate crosswise across the drift with wrong momentum fails") {
        const double x_offset = 0.1;
        // calculate angle and path length for diagonal crossing from
        // -x_offset to +x_offset over the drift length
        const double proton_path_length = 
            std::sqrt(drift_length*drift_length + 4.0*x_offset*x_offset);
        const double xprime = 2.0*x_offset/proton_path_length;

        probe.set_x(-x_offset);
        probe.set_npx(xprime);
        probe.set_ndp(1.0e-2);
        my_beamline->propagate(probe);

        CHECK_FALSE( probe.get_x() == Approx(x_offset) );
        CHECK( probe.get_y() == Approx(0.0).margin(1.0e-15) );
        CHECK_FALSE( probe.get_cdt() == Approx(proton_path_length/probe_beta) );
    }

    SECTION("Check propagation time with different momentum") {
        const double dpp_offset = 0.04;

        probe.set_ndp(dpp_offset);
        const double new_probe_beta = probe.Beta();

        my_beamline->propagate(probe);

        CHECK( probe.get_cdt() == Approx(drift_length/new_probe_beta) );
    }

    SECTION("check drift map") {
        // create 3rd order jet environment
        JetParticle::createStandardEnvironments( 3 );

        JetProton jetprobe(probe);

        my_beamline->propagate(jetprobe);
        MatrixD map(jetprobe.State().Jacobian());

        // the first order map for the drift is:
        // x_f = x_i + xp_i * L

        std::cout << "the map: " << std::endl;
        std::cout << map << std::endl;

        // The xp linear coefficient is the drift length
        CHECK( drift_length == Approx(map[0][3]));

        // check the third order xp**2 and xp*yp**2 coefficients
        // x = x + xp * (1 + 0.5*xp**2 + 0.5*yp**2) * L + ...

        TJetVector<double> mapping(jetprobe.State());
    
        EnvPtr<double> env = mapping.Env();
        int got_em = 2;
        for (TJet<double>::const_iterator jet_it = mapping(0).begin(); jet_it != mapping(0).end(); ++jet_it) {
            if ((jet_it->exponents(env)(0) == 0) &&
                (jet_it->exponents(env)(1) == 0) &&
                (jet_it->exponents(env)(2) == 0) &&
                (jet_it->exponents(env)(3) == 3) &&
                (jet_it->exponents(env)(4) == 0) &&
                (jet_it->exponents(env)(5) == 0)) {
                // this is the xp**3 term
                --got_em;
                CHECK(jet_it->coefficient() == Approx(0.5*drift_length));
            } else if (
                (jet_it->exponents(env)(0) == 0) &&
                (jet_it->exponents(env)(1) == 0) &&
                (jet_it->exponents(env)(2) == 0) &&
                (jet_it->exponents(env)(3) == 1) &&
                (jet_it->exponents(env)(4) == 2) &&
                (jet_it->exponents(env)(5) == 0)) {
                // this is the xp * yp**2 term
                --got_em;
                CHECK(jet_it->coefficient() == Approx(0.5 * drift_length));
            }
        }
        CHECK(got_em == 0);
    }
}
