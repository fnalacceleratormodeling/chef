#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>

#include <beamline/beamline.h>
#include <beamline/Particle.h>
#include <beamline/sbend.h>
#include <beamline/drift.h>
#if 0
#include <beamline/rbend.h>
#include <beamline/CF_sbend.h>
#include <beamline/CF_rbend.h>
#endif

TEST_CASE("Sbend", "[Sbend]")
{
    const double pc = 2.784435311;
    const double sbend_length = 1.09812;
    const double sbend_angle = 0.01567741327;
    const double sbend_margin = 1.0e-11;

    // Need the momentum and brho to compute strength from angle
    Proton probe;
    probe.SetReferenceMomentum(pc);
    double probe_beta = probe.ReferenceBeta();
    double brho = probe.ReferenceBRho();

    const double sbend_strength = sbend_angle * brho/sbend_length;

    // can I construct an sbend?
    SBendPtr my_bend(new sbend("foo", sbend_length, sbend_strength, sbend_angle));
    REQUIRE(my_bend);

    // can I construct a beamline?
    BmlPtr my_beamline(new beamline("bar"));
    REQUIRE(my_beamline);

    // can I append my drift to the beamline and does it
    // get the element count right?
    my_beamline->append(my_bend);
    REQUIRE( my_beamline->countHowMany() == 1 );
    
    SECTION("propagating 0 particle") {
        my_beamline->propagate(probe);
        CHECK(probe.get_x() == Approx(0.0).margin(sbend_margin));
        CHECK(probe.get_npx() == Approx(0.0).margin(sbend_margin));
        CHECK(probe.get_y() == Approx(0.0).margin(sbend_margin));
        CHECK(probe.get_npy() == Approx(0.0).margin(sbend_margin));
        CHECK(probe.get_ndp() == Approx(0.0).margin(sbend_margin));

        // check time coordinate c*time is L/beta
        CHECK(probe.get_cdt() == Approx(sbend_length/probe_beta));
    }

    SECTION("propagating 0 particle with wrong momentum gives different results") {
        const double sbend_margin = 1.0e-11;
        const double dpp_offset = 0.04;
        
        probe.set_ndp(dpp_offset);
        my_beamline->propagate(probe);
        CHECK_FALSE(probe.get_x() == Approx(0.0).margin(sbend_margin));
        CHECK_FALSE(probe.get_npx() == Approx(0.0).margin(sbend_margin));
        // y coordinates are unaffected by different momentum
        CHECK(probe.get_y() == Approx(0.0).margin(sbend_margin));
        CHECK(probe.get_npy() == Approx(0.0).margin(sbend_margin));
        // momentum is not changed by magnetic field
        CHECK(probe.get_ndp() == Approx(dpp_offset).margin(sbend_margin));

        // check time coordinate c*time is L/beta
        CHECK_FALSE(probe.get_cdt() == Approx(sbend_length/probe_beta));
    }

    SECTION("propagate off momentum particle") {
        const double radius = sbend_length/sbend_angle;
        const double dpp_offset = 0.04;
        const double sbend_tolerance = 1.0e-8;
        
        probe.set_ndp(dpp_offset);
        double const beta1 = probe.Beta();
        /*
         * on-momentum particle moves around the circle of radius
         * Make the center of the circle (0, -radius)
         * the nominal particle travels length l.  The sbend edge will be
         * the line that includes the (0, -radius) and the end of travel.
         */
        double sbend_exit_x = radius * std::sin(sbend_angle);
        double sbend_exit_y = -radius * (1.0 - std::cos(sbend_angle));
    
        // check propagation of off momentum particle
        my_bend->propagate(probe);

        double radius1 = radius * (1 + dpp_offset);
        double delta_radius = radius1-radius;
        double cot_theta = std::cos(sbend_angle)/std::sin(sbend_angle);
        /*
         * particle 1 exit point at the intersection of the circle of radius
         * radius1 centered at
         * (0, -radius1) with line from (0,-radius), (sbend_exit_x, sbend_exit_y)
         * which works out to be
         * y = -radius + cos(angle)/sin(angle)*x
         *
         *  intersection with circle
         * 
         * x**2 + (y + radius1)**2 = radius1**2
         *
         * substitute
         * (1+ct**2)*x**2 + 2*ct*dr*x + dr**2-radius1**2 = 0 and
         * solve quadratic equation
         *
         * px/pref calculation L. Michelotti:
         * px/pref = (dp/pref) * sin(theta_0)
         *
         */
        double a = 1.0 + cot_theta*cot_theta;
        double b = 2.0*cot_theta*delta_radius;
        double c = delta_radius*delta_radius - radius1*radius1;
        // quadratic solution (for 1/x, inverted)
        double sbend_exit_x1 = 2*c/(-b - std::sqrt(b*b - 4*a*c));
        double sbend_exit_y1 = -radius + cot_theta*sbend_exit_x1;
        double d_offset = std::sqrt(std::pow(sbend_exit_x-sbend_exit_x1,2) +
                                    std::pow(sbend_exit_y-sbend_exit_y1,2));
        // offset distance is the x offset in accelerator coordinates
        CHECK( d_offset == Approx( probe.get_x()).epsilon(sbend_tolerance) );

        // check px/pref
        double px_over_pref = dpp_offset * std::sin(sbend_angle);
        CHECK( probe.get_npx() == Approx(px_over_pref).epsilon(sbend_tolerance));

        // check path length difference
        // path length for particle 0 is just sbend_length
        // calculate path length for particle 1 on its circular trajectory
        // origin of circle is (0, -radius1)
        // vector of particle start position is (0, radius1)
        // vector of particle end position is (sbend_exit_x1, sbend_exit_y1-(-radius1))
        // calculate dot product to get angle
        double v0dotv1 = sbend_exit_y1+radius1; // *radius1 which will cancel
        double v1_norm = std::sqrt(pow(sbend_exit_x1, 2) +
                                   pow(sbend_exit_y1+radius1, 2));
        double theta1 = std::acos(v0dotv1/v1_norm);  // * 1/radius1
        double sbend_length1 = theta1 * radius1;
        CHECK( probe.get_cdt() == Approx(sbend_length1/beta1).epsilon(sbend_tolerance));

    }

    SECTION("propagate x offset particle") {
        const double x_offset = .001;
        const double sbend_tolerance = 1.0e-8;
        const double radius = sbend_length/sbend_angle;
        double cot_theta = std::cos(sbend_angle)/std::sin(sbend_angle);

        probe.set_x(x_offset);
        my_bend->propagate(probe);

        /*
         * on-momentum particle moves around the circle of radius
         * Make the center of the circle (0, -radius)
         * the nominal particle travels length l.  The sbend edge will be
         * the line that includes the (0, -radius) and the end of travel.
         */
        double sbend_exit_x = radius * std::sin(sbend_angle);
        double sbend_exit_y = -radius * (1.0 - std::cos(sbend_angle));

        // In this case, the trajectory follows a circle of original radius
        // but centered on (0, -R+x_offset).  The exit point is the intersection of that
        // circle with the line of close cot(theta) passing through (0, -R) which is the solution
        // of the quadratic equation
        // (1+ct**2)*x**2 - 2*ct*x_offset*x + x_offset**2-radius**2 = 0
        double a = 1.0 + cot_theta*cot_theta;
        double b = -2.0*cot_theta*x_offset;
        double c = x_offset*x_offset - radius*radius;
        // quadratic solution (for 1/x, inverted)
        double sbend_exit_x2 = 2*c/(-b - std::sqrt(b*b - 4*a*c));
        double sbend_exit_y2 = -radius + cot_theta*sbend_exit_x2;
        double d_offset = std::sqrt(std::pow(sbend_exit_x-sbend_exit_x2,2) +
                             std::pow(sbend_exit_y-sbend_exit_y2,2));
        // offset distance is the x offset in accelerator coordinates
        CHECK( probe.get_x() == Approx(d_offset).epsilon(sbend_tolerance) );

        // get pathlength
        // start position vector is (0, -radius)
        // final position vector is (sbend_exit_x2, sbend_exit_y2-(-radius+x_offset)
        double v0dotv2 = sbend_exit_y2+radius-x_offset; // times radius
        // double v2norm = std::sqrt(pow(sbend_exit_x2, 2) +
    	// 	                  pow(sbend_exit_y2-radius+x_offset, 2));
        double theta2 = std::acos(v0dotv2/radius);
        double sbend_length2 = theta2 * radius;
        CHECK( probe.get_cdt() == Approx(sbend_length2/probe_beta).epsilon(sbend_tolerance) );

        // check x'.  Transverse momentum is the sin of the angle between the
        // vector normal to the sbend exit face and the particle trajectory vector.
        // calculate with the cross product.
        double exit_normal_x = (sbend_exit_y2-(-radius+x_offset))/radius;
        double exit_normal_y = -sbend_exit_x2/radius;
        double face_normal_x = std::cos(sbend_angle);
        double face_normal_y = -std::sin(sbend_angle);
        // cross product
        double exit_px = face_normal_x*exit_normal_y - face_normal_y*exit_normal_x;
        CHECK( probe.get_npx() == Approx(exit_px).epsilon(sbend_tolerance) );

    }

}
