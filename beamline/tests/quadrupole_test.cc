#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>

#include <beamline/beamline.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/quadrupole.h>

TEST_CASE("Quadrupole", "[Quadrupole]")
{
    const double pc = 2.784435311;
    const double quad_length = 1.25;
    const double focus = 7.142857;

    // create 1st order jet environment
    JetParticle::createStandardEnvironments( 1 );

    Proton probe;
    probe.SetReferenceMomentum(pc);
    double brho = probe.ReferenceBRho();

    // kL would be 1/focus
    const double k1 = 1.0/(focus * quad_length);

    // B'L would be brho * 1/focus
    const double bprime = brho/(focus * quad_length);

    // can I construct a quadrupole?
    QuadrupolePtr my_quad(new quadrupole("foo", quad_length, bprime));
    REQUIRE(my_quad);

    // can I construct a beamline?
    BmlPtr my_beamline(new beamline("bar"));
    REQUIRE(my_beamline);

    // can I append my drift to the beamline and does it
    // get the element count right?
    my_beamline->append(my_quad);
    REQUIRE( my_beamline->countHowMany() == 1 );
    
    double probe_beta = probe.ReferenceBeta();

    SECTION("propagating 0 particle") {
        my_beamline->propagate(probe);
        CHECK(probe.get_x() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_npx() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_y() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_npy() == Approx(0.0).margin(1.0e-14));
        CHECK(probe.get_ndp() == Approx(0.0).margin(1.0e-14));

        // check time coordinate c*time is L/beta
        CHECK(probe.get_cdt() == Approx(quad_length/probe_beta));
    }

    SECTION("quadrupole map") {
        // calculate map through quadrupole
        JetProton jetprobe(probe);

        my_beamline->propagate(jetprobe);
        MatrixD map(jetprobe.State().jacobian());
        std::cout << "the map: " << map << std::endl;

        // To first order, the mapping of a quadrupole with parameter k
        // and length L is:
        //
        //     \cos \sqrt{k} L |  \frac{1}{k} \sin \sqrt{k} L | |
        //     -\sqrt{k} \sin \sqrt{k} L | \cos \sqrt{k} L | | 
        //     | | \cosh \sqrt{k} L | \frac{1}{k} \sinh \sqrt{k} L
        //     | | \sqrt{k} \sinh \sqrt{k} L | \cosh \sqrt{k} L

        // remember CHEF's ordering of elements is x y z xp yp zp

        double rootk = std::sqrt(k1);
        double rootkL = rootk * quad_length;
        double m00 = std::cos(rootkL);
        double m01 = (1.0/rootk) * std::sin(rootkL);
        double m10 = -rootk * std::sin(rootkL);
        double m11 = std::cos(rootkL);

        double m22 = std::cosh(rootkL);
        double m23 = (1.0/rootk) * std::sinh(rootkL);
        double m32 = rootk * std::sinh(rootkL);
        double m33 = std::cosh(rootkL);

        CHECK(map[0][0] == Approx(m00));
        CHECK(map[0][3] == Approx(m01));
        CHECK(map[3][0] == Approx(m10));
        CHECK(map[3][3] == Approx(m11));

        CHECK(map[1][1] == Approx(m22));
        CHECK(map[1][4] == Approx(m23));
        CHECK(map[4][1] == Approx(m32));
        CHECK(map[4][4] == Approx(m33));

    }
}
