/*************************************************************************
**************************************************************************
**************************************************************************
******
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and
******             synchrotrons.
******
******  File:      rfcavity.cc
******
******  Author:    Leo Michelotti
******             Phone: (630) 840 4956
******             Email: michelotti@fnal.gov
******
******  Copyright (c) Universities Research Association, Inc./ Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000.
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******  Apr 2007    ostiguy@fnal.gov
******  - support for reference counted elements
******  - visitor interface takes advantage of compiler dynamic typing
******  
******  Dec 2007    ostiguy@fnal.gov
******  - new typesafe propagators
******
******  Apr 2008    michelotti@fnal.gov
******  - added placeholder rfcavity::setLength method
******
******  Apr 2010    michelotti@fnal.gov
******  - upgraded rfcavity::setStrength method to one that creates
******    a new propagator functor.
******    : memory leak test passed.
******
******  Dec 2012    michelotti@fnal.gov
******  - added functionality for multiple harmonics
******    : simple-minded implementation; should be improved some day.
****** 
******  Jul 2014    michelotti@fnal.gov
******  - added functionality for cavity with frequency not
******    multiple of the principal harmonic
******    : to simulate slip stacking.
******
**************************************************************************
*************************************************************************/

#include <iomanip>
#include <basic_toolkit/GenericException.h>
#include <beamline/RFCavityPropagators.h>
#include <beamline/rfcavity.h>
#include <beamline/beamline.h>
#include <beamline/drift.h>
#include <beamline/BmlVisitor.h>
#include <beamline/RefRegVisitor.h>
#include <beamline/marker.h>

using namespace std;
using FNAL::pcerr;
using FNAL::pcout;

// **************************************************
//   class rfcavity
// **************************************************

rfcavity::rfcavity( const char* name_arg)
: bmlnElmnt(name_arg, 1.0, 0.0),
  w_rf_(0.0),
  initial_w_rf_(0.0),
  phi_s_(0.0),
  sin_phi_s_(0.0),
  Q_(0.0),
  R_(0.0),
  h_(-1.0),
  my_harmonic_parameters_(),
  displaced_phase_slip_(0.0),
  cumulative_displaced_phase_slip_(0.0)
{
  my_harmonic_parameters_.push_back( multiple_harmonic_parameters( 1, 1.0, 0.0 ) );
  propagator_ = PropagatorPtr(new Propagator() );
  propagator_->setup(*this);
}
  
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

rfcavity::rfcavity( const char* name_arg, // name
                    double lng_arg,    // length [m]
                    double f_arg,      // rf frequency
                    double eV_arg,     // rf voltage
                    double phi_s_arg,  // synchronous phase
                    double Q_arg,      // Quality factor
                    double R_arg       // shunt impedance
                  ) 
: bmlnElmnt( name_arg, lng_arg, eV_arg*1.0e-9 ),
  w_rf_(MATH_TWOPI*f_arg),
  initial_w_rf_( w_rf_ ),
  phi_s_(phi_s_arg),
  sin_phi_s_(sin(phi_s_arg)),
  Q_(Q_arg), 
  R_(R_arg),
  h_(-1.0),
  my_harmonic_parameters_(),
  displaced_phase_slip_(0.0),
  cumulative_displaced_phase_slip_(0.0)
{
  my_harmonic_parameters_.push_back( multiple_harmonic_parameters( 1, 1.0, 0.0 ) );
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

rfcavity::rfcavity( rfcavity const& x ) 
: bmlnElmnt( x ),
  w_rf_(x.w_rf_),
  initial_w_rf_(x.initial_w_rf_),
  phi_s_(x.phi_s_),
  sin_phi_s_(x.sin_phi_s_),
  Q_(x.Q_),
  R_(x.R_),
  h_(x.h_),
  my_harmonic_parameters_( x.my_harmonic_parameters_ ),
  displaced_phase_slip_(x.displaced_phase_slip_),
  cumulative_displaced_phase_slip_(x.cumulative_displaced_phase_slip_),
  propagator_ (x.propagator_->Clone())
{
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

rfcavity::~rfcavity()
{}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::Split( double, ElmPtr& a, ElmPtr& b ) const
{
  (*pcerr) <<   "*** WARNING ****: "
              "\n*** WARNING ****: "  << __FILE__ << "," << __LINE__
           << "\n*** WARNING ****: void " << Type() << "::Split( double, ElmPtr&, ElmPtr& ) const"
              "\n*** WARNING ****: Splitting a " << Type() << " is forbidden in this version."
              "\n*** WARNING ****: " 
           << std::endl;
  ostringstream uic;
  uic  <<   "Splitting a " << Type() << " is forbidden in this version.";
  throw( GenericException( __FILE__, __LINE__, 
         "void rfcavity::Split( double, ElmPtr&, ElmPtr& ) const",
         uic.str().c_str() ) );
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

ostream& rfcavity::writeTo(ostream& os) 
{
  os << OSTREAM_DOUBLE_PREC 
     << (w_rf_/MATH_TWOPI)
     << " " << phi_s_
     << " " << Q_
     << " " << R_
     << " " << h_
     << '\n';
  return os;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

istream& rfcavity::readFrom(istream& is) 
{
  double w;
  is >> w 
     >> phi_s_ 
     >> Q_ 
     >> R_
     >> h_;
  w_rf_ = w*MATH_TWOPI;
  sin_phi_s_ = sin(phi_s_);
  propagator_->setup(*this);
  return is;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


const char* rfcavity::Type() const 
{
  return "rfcavity"; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


bool    rfcavity::isMagnet() const 
{
  return false;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getReferenceTime()    const 
{
  double value = 0;

  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    value += (*it)->getReferenceTime();
  }

  return value;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setStrength( double sss)
{
  bmlnElmnt::setStrength(sss);
  propagator_->setup(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setLength( double )
{
  ostringstream methodIdent;
  methodIdent << "void " << Type() << "::setLength( double )";
  
  (*pcerr) <<   "*** ERROR ****: "
              "\n*** ERROR ****: "  << __FILE__ << "," << __LINE__
           << "\n*** ERROR ****: void " << Type() << "::setLength( double )"
              "\n*** ERROR ****: Resetting the length of " 
           << Type() << " is not allowed in this version."
              "\n*** ERROR ****: " 
           << std::endl;

  ostringstream uic;
  uic << "Resetting the length of " << Type() << " is not allowed in this version.";
  throw( GenericException( __FILE__, __LINE__, 
           methodIdent.str().c_str(),
           uic.str().c_str() ) );
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setHarmonicNumber( double n )
{
  if( 0 < n ) { h_ = n; }

  ThinRFCavityPtr q;
  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    if( (q = boost::dynamic_pointer_cast<thinrfcavity>(*it) )) { q->setHarmonicNumber( n ); }
  }
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setHarmonicNumber( int n )
{
  setHarmonicNumber( static_cast<double>(n) );
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setFrequency( double f )
{
  if( f > 0 ) {
    w_rf_ = MATH_TWOPI*f;
  }

  initial_w_rf_ = w_rf_;
  displaced_phase_slip_ = 0.0;
  cumulative_displaced_phase_slip_ = 0.0;

  ThinRFCavityPtr q;

  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    if( (q = boost::dynamic_pointer_cast<thinrfcavity>(*it) )) { q->setFrequency( f ); }
  }
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setDisplacedFrequency( double f, double T )
{
  if( w_rf_ <= 0.0 ) {
    ostringstream uic;
    uic << "w_rf_ = " << w_rf_ << " indicates not set previously.";
    throw( GenericException( __FILE__, __LINE__, 
           "void rfcavity::setDisplacedFrequency( double, double )",
           uic.str().c_str() ) );
  }
  if( initial_w_rf_ <= 0.0 ) {
    ostringstream uic;
    uic << "Initial w_rf_ = " << initial_w_rf_ << "; something went horribly wrong.";
    throw( GenericException( __FILE__, __LINE__, 
           "void rfcavity::setDisplacedFrequency( double, double )",
           uic.str().c_str() ) );
  }
  if( T <= 0.0 ) {
    ostringstream uic;
    uic << "The fiducial revolution time must be positive! What are you thinking??";
    throw( GenericException( __FILE__, __LINE__, 
           "void rfcavity::setDisplacedFrequency( double, double )",
           uic.str().c_str() ) );
  }

  if( f > 0.0 ) { w_rf_ = MATH_TWOPI*f; }

  displaced_phase_slip_ = ( w_rf_ - initial_w_rf_ )*T;
  while( M_PI < displaced_phase_slip_    ) { displaced_phase_slip_ -= M_TWOPI; }
  while( displaced_phase_slip_ <= - M_PI ) { displaced_phase_slip_ += M_TWOPI; }

  cumulative_displaced_phase_slip_ = 0.0;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setFrequencyRelativeTo( double f )
{
  if( (0 < f) && (0 < h_) ) {
    w_rf_ = MATH_TWOPI*h_*f;
  }

  initial_w_rf_ = w_rf_;
  displaced_phase_slip_ = 0.0;
  cumulative_displaced_phase_slip_ = 0.0;

  ThinRFCavityPtr q;

  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    if( (q =boost::dynamic_pointer_cast<thinrfcavity>(*it) )) { q->setFrequencyRelativeTo( f ); }
  }
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setRadialFrequency( double omega )
{
  ThinRFCavityPtr q;

  if( omega > 0 ) {
    w_rf_ = omega;
  }

  initial_w_rf_ = w_rf_;

  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    if( (q = boost::dynamic_pointer_cast<thinrfcavity>(*it) )) q->setRadialFrequency( w_rf_ );  
  }
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setRadialFrequencyRelativeTo( double omega )
{
  ThinRFCavityPtr q;

  if( (omega >0 ) && (h_ > 0) ) {
    w_rf_ = h_*omega;
  }

  initial_w_rf_ = w_rf_;

  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    if( (q=boost::dynamic_pointer_cast<thinrfcavity>(*it))) q->setRadialFrequency( w_rf_);
  }

}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setPhi( double angle )
{
  phi_s_     = angle;
  sin_phi_s_ = sin(angle);
  propagator_->setup(*this);
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setQ( double Q )
{
  Q_     = Q;
  propagator_->setup(*this);
};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setR( double R )
{
  R_ = R;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::accept( BmlVisitor& v ) 
{ 
  v.visit( *this ); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::accept( ConstBmlVisitor& v ) const 
{ 
  v.visit( *this ); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getPhi() const
{ 
  return phi_s_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getRadialFrequency() const
{ 
 return w_rf_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getDesignEnergyGain()   const
{
    return strength_*sin_phi_s_; 
}
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double  rfcavity::getQ()  const
{ 
  return Q_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getR() const
{ 
  return R_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getHarmonicNumber() const
{ 
  return h_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void rfcavity::usePropagator( PropagatorPtr& x )
{
  propagator_ = PropagatorPtr( x->Clone() );
  propagator_->setup( *this );
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::localPropagate( Particle& p) 
{
  (*propagator_)(*this, p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::localPropagate( JetParticle& p) 
{
  (*propagator_)(*this, p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::localPropagate( ParticleBunch& b) 
{
  (*propagator_)(*this, b);
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::localPropagate( JetParticleBunch& b) 
{
  (*propagator_)(*this, b);
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::addHarmonic( int harmonic, double relativeStrength, double phase )
{
  my_harmonic_parameters_.push_back( multiple_harmonic_parameters( harmonic, relativeStrength, phase ) );
  propagator_->setup(*this);
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::turnUpdate()
{
  cumulative_displaced_phase_slip_ += displaced_phase_slip_;
  while( M_PI < cumulative_displaced_phase_slip_    ) { cumulative_displaced_phase_slip_ -= M_TWOPI; }  // Hopelessly
  while( cumulative_displaced_phase_slip_ <= - M_PI ) { cumulative_displaced_phase_slip_ += M_TWOPI; }  // paranoid way
                                                                                                          // of doing this.

  ThinRFCavityPtr q;
  for( beamline::iterator it = bml_->begin(); it != bml_->end();  ++it ) {
    if( (q = boost::dynamic_pointer_cast<thinrfcavity>(*it) )) { q->turnUpdate(); }
  }
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double rfcavity::getCumulativeDisplacedPhaseSlip()
{
    return cumulative_displaced_phase_slip_;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rfcavity::setCumulativeDisplacedPhaseSlip(double cum_phase_slip)
{
    cumulative_displaced_phase_slip_ = cum_phase_slip;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


// **************************************************
//   class thinrfcavity 
// **************************************************

thinrfcavity::thinrfcavity(const char *name_arg) 
: bmlnElmnt(name_arg, 0.0, 0.0),
  w_rf_( 0.0 ),
  initial_w_rf_(0.0),
  phi_s_( 0.0 ),
  sin_phi_s_( 0.0 ),
  Q_( 0.0 ),
  R_( 0.0 ),
  h_( -1.0 ),
  my_harmonic_parameters_(),
  displaced_phase_slip_(0.0),
  cumulative_displaced_phase_slip_(0.0)
{
  my_harmonic_parameters_.push_back( rfcavity::multiple_harmonic_parameters( 1, 1.0, 0.0 ) );
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}
  
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

thinrfcavity::thinrfcavity(const char * name_arg, // name
                           double f_arg,      // rf frequency
                           double eV_arg,     // rf voltage
                           double phi_s_arg,  // synchronous phase
                           double Q_arg,      // Quality factor
                           double R_arg       // shunt impedance
                           ) 
: bmlnElmnt( name_arg, 0.0, eV_arg*1.0e-9 ),
  w_rf_( MATH_TWOPI*f_arg ),
  initial_w_rf_( w_rf_ ),
  phi_s_( phi_s_arg ),
  sin_phi_s_( sin(phi_s_) ),
  Q_( Q_arg ),
  R_( R_arg ),
  h_( -1.0 ),
  my_harmonic_parameters_(),
  displaced_phase_slip_(0.0),
  cumulative_displaced_phase_slip_(0.0)
{
  my_harmonic_parameters_.push_back( rfcavity::multiple_harmonic_parameters( 1, 1.0, 0.0 ) );
  propagator_ = PropagatorPtr(new Propagator() );
  propagator_->setup(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

thinrfcavity::thinrfcavity( const thinrfcavity& x ) 
: bmlnElmnt( x ),
  w_rf_( x.w_rf_ ),
  initial_w_rf_(x.initial_w_rf_),
  phi_s_( x.phi_s_ ),
  sin_phi_s_( x.sin_phi_s_ ),
  Q_( x.Q_ ),
  R_( x.R_ ),
  h_( x.h_ ), 
  my_harmonic_parameters_( x.my_harmonic_parameters_ ),
  displaced_phase_slip_(x.displaced_phase_slip_),
  cumulative_displaced_phase_slip_(x.cumulative_displaced_phase_slip_),
  propagator_ (x.propagator_->Clone() )
{
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


thinrfcavity::~thinrfcavity()
{}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

ostream& thinrfcavity::writeTo(ostream& os) 
{
  os << OSTREAM_DOUBLE_PREC
     << w_rf_/MATH_TWOPI
     << " " <<  phi_s_
     << " " << Q_
     << " " << R_
     << " " << h_
     << '\n';
  return os;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


istream& thinrfcavity::readFrom(istream& is) 
{
  double w;
  is >> w 
     >> phi_s_ 
     >> Q_ 
     >> R_
     >> h_;
  w_rf_      = w*MATH_TWOPI;
  sin_phi_s_ = sin(phi_s_);
  return is;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

const char* thinrfcavity::Type() const 
{
  return "thinrfcavity"; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

bool    thinrfcavity::isMagnet() const 
{

  return false;

}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::setHarmonicNumber( double n )
{
  if( 0 < n ) { h_ = n; }
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void thinrfcavity::setHarmonicNumber( int n )
{
  setHarmonicNumber( static_cast<double>(n) );
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void thinrfcavity::setFrequency( double f )
{
  if( f > 0 ) {
    w_rf_ = MATH_TWOPI*f;
  }

  initial_w_rf_ = w_rf_;
  displaced_phase_slip_ = 0.0;
  cumulative_displaced_phase_slip_ = 0.0;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::setDisplacedFrequency( double f, double T )
{
  if( w_rf_ <= 0.0 ) {
    ostringstream uic;
    uic << "w_rf_ = " << w_rf_ << " indicates not set previously.";
    throw( GenericException( __FILE__, __LINE__, 
           "void rfcavity::setDisplacedFrequency( double, double )",
           uic.str().c_str() ) );
  }
  if( initial_w_rf_ <= 0.0 ) {
    ostringstream uic;
    uic << "Initial w_rf_ = " << initial_w_rf_ << "; something went horribly wrong.";
    throw( GenericException( __FILE__, __LINE__, 
           "void rfcavity::setDisplacedFrequency( double, double )",
           uic.str().c_str() ) );
  }
  if( T <= 0.0 ) {
    ostringstream uic;
    uic << "The fiducial revolution time must be positive! What are you thinking??";
    throw( GenericException( __FILE__, __LINE__, 
           "void rfcavity::setDisplacedFrequency( double, double )",
           uic.str().c_str() ) );
  }

  if( f > 0.0 ) { w_rf_ = MATH_TWOPI*f; }

  displaced_phase_slip_ = ( w_rf_ - initial_w_rf_ )*T;
  while( M_PI < displaced_phase_slip_    ) { displaced_phase_slip_ -= M_TWOPI; }
  while( displaced_phase_slip_ <= - M_PI ) { displaced_phase_slip_ += M_TWOPI; }

  cumulative_displaced_phase_slip_ = 0.0;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void thinrfcavity::setFrequencyRelativeTo( double f )
{
  if( (f > 0) && (h_ > 0) ) {
    w_rf_ = MATH_TWOPI*h_*f;
  }

  initial_w_rf_ = w_rf_;
  displaced_phase_slip_ = 0.0;
  cumulative_displaced_phase_slip_ = 0.0;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::setRadialFrequency( double omega )
{
  if( omega > 0 ) {
    w_rf_ = omega;
  }

  initial_w_rf_ = w_rf_;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::setRadialFrequencyRelativeTo( double omega )
{
  if( (omega > 0) && (h_>0) ) {
    w_rf_ = h_*omega;
  }

  initial_w_rf_ = w_rf_;
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::addHarmonic( int harmonic, double relativeStrength, double phase )
{
  my_harmonic_parameters_.push_back( rfcavity::multiple_harmonic_parameters( harmonic, relativeStrength, phase ) );
  propagator_->setup(*this);  // Should be nothing to do here, I imagine.
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::setPhi( double angle )
{
  phi_s_     = angle;
  sin_phi_s_ = sin(angle);
};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  thinrfcavity::accept( BmlVisitor& v )
{
 
  v.visit(*this); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void  thinrfcavity::accept( ConstBmlVisitor& v ) const
{
  v.visit(*this); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double thinrfcavity::getPhi()              const
{ 
 return phi_s_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double thinrfcavity::getRadialFrequency()        const
{ 
  return w_rf_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double thinrfcavity::getQ()                const
{ 
  return Q_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double thinrfcavity::getR()                const
{ 
  return R_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double thinrfcavity::getHarmonicNumber()   const
{ 
  return h_; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void thinrfcavity::usePropagator( PropagatorPtr& x )
{
  propagator_ = PropagatorPtr( x->Clone() );
  propagator_->setup( *this );
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::localPropagate( Particle& p) 
{
  (*propagator_)(*this, p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::localPropagate( JetParticle& p) 
{
  (*propagator_)(*this, p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::localPropagate( ParticleBunch& b) 
{
  (*propagator_)(*this, b);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::localPropagate( JetParticleBunch& b) 
{
  (*propagator_)(*this, b);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::turnUpdate()
{
  cumulative_displaced_phase_slip_ += displaced_phase_slip_;
  while( M_PI < cumulative_displaced_phase_slip_    ) { cumulative_displaced_phase_slip_ -= M_TWOPI; }  // Hopelessly
  while( cumulative_displaced_phase_slip_ <= - M_PI ) { cumulative_displaced_phase_slip_ += M_TWOPI; }  // paranoid way
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

double thinrfcavity::getCumulativeDisplacedPhaseSlip()
{
    return cumulative_displaced_phase_slip_;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void thinrfcavity::setCumulativeDisplacedPhaseSlip(double cum_phase_slip)
{
    cumulative_displaced_phase_slip_ = cum_phase_slip;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
