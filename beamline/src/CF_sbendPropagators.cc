/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      CF_sbendPropagators.cc
******
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******
******  Authors:   Leo Michelotti         michelotti@fnal.gov
******             Jean-Francois Ostiguy  ostiguy@fnal.gov
******             Eric Stern             egstern@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******  
******  Apr 2008            michelotti@fnal.gov
******  - bug fix: changed arguments sent to sbend constructors 
******    in CF_sbend::Propagator::setup
******  - nullified edge effects from internal bends.
******    : edge effects to be handled by elements usedge and dsedge only
******
******  Dec 2014            michelotti@fnal.gov
******  - removed the "KLUDGE" that prevented the .setup routine
******    from working more than once on the same or a cloned CF_rbend.
******    : as a reminder, the issue of multiply redundant setups
******      has never been handled satisfactorily.
******  
******  Jan 2015            michelotti@fnal.gov
******  - bug fix: added code to the .setup routine for reinitializing
******    a pre-existing CF_sbend with fewer than two edge elements.
******    : repeat "as a reminder" from Dec 2014 entry (above).
******  
******  Apr 2015            michelotti@fnal.gov
******  - added option of using dynamically calculated entry and exit
******    angles in the edge propagators, instead of the angles 
******    "hard-wired" by RefRegVisitor. To enable this option,
******    pass a -DNO_FIXED_ENTRY_ANGLE macro definition to the compiler.
******    (See update below.)
******
******  Jan 2016           egstern@fnal.gov
******  - replaced thinQuad elements with the new InducedKick,
******    which uses expressions for induced, effective magnetic fields 
******    for cylindrical geometry, derived independently
******    by Timofey Zolkin (2015) and Edwin McMillan (1975).
******
******  Aug 2016         michelotti@fnal.gov
******  - NO_FIXED_ENTRY_ANGLE is now defined as true in the header
******    file Edge.h. The previous default behavior is changed. Getting
******    it back is now not a compile time option but must be done
******    by rewriting the line in the header file before compilation.
******    For further explanation, read comments added to Edge.h.
******
**************************************************************************
*************************************************************************/

#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/quadrupole.h>
#include <beamline/sextupole.h>
#include <beamline/InducedKick.h>
#include <beamline/InducedSextupoleKick.h>
#include <beamline/beamline.h>
#include <beamline/Edge.h>
#include <beamline/Bend.h>
#include <beamline/sbend.h>
#include <beamline/CF_sbendPropagators.h>
#include <beamline/beamline.h>
#include <iostream>

namespace {

  Particle::PhaseSpaceIndex i_cdt = Particle::cdtIndex;

template<typename Particle_t>
void propagateSome( beamline::iterator bit_start, beamline::iterator bit_end, Particle_t &p)
{

    for (beamline::iterator it = bit_start; it != bit_end; ++it) {
        (*it)->localPropagate(p);
    }
}

template<typename Particle_t>
void propagate( CF_sbend& elm, Particle_t&  p)
{
  
  typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

  State_t& state = p.State();

  BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( elm );

  propagateSome(bml->begin(), bml->end(), p);

  state[i_cdt] -= elm.getReferenceTime(); 
}

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------
#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void propagate( CF_sbend& elm,    Particle& p );
template void propagate( CF_sbend& elm, JetParticle& p );

#endif
//-----------------------------------------------------------------------------------

} // namespace


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void CF_sbend::Propagator::setup( CF_sbend& arg ) 
{
  bool hasEntryEdge = false;
  bool hasExitEdge  = false;
  bool hasOldBml    = false;

  BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr(arg);
  double const bend_radius = arg.Length()/arg.getBendAngle();

  if( bml ) {
    hasOldBml    = true;
    hasEntryEdge = ( typeid(*(bml->firstElement())) == typeid(Edge) );
    hasExitEdge  = ( typeid(*(bml->lastElement()))  == typeid(Edge) );
  }

  //----------------------------------------------------------------------------
  // NOTE: the proportions below come from a quadrature rule meant to minimize 
  //       the error when a magnet is split into 4 parts. See R. Talman 
  //         
  // 2*6/15  *  (L/4) +   3*16/15 * (L/4)  = 60/15 * ( L/4) =  L
  // 2* ends +   body     =  L  
  //----------------------------------------------------------------------------       

  double field       =  arg.Strength();

  double frontLength =   (6.0/15.0)*( arg.Length()/(4.0*n_) );
  double sepLength   =  (16.0/15.0)*( arg.Length()/(4.0*n_) );
  
  double quadStrength = arg.getQuadrupole();
  double sextStrength = arg.getSextupole();

  #if NO_FIXED_ENTRY_ANGLE
  Edge      usedge( "",   field );
  Edge      dsedge( "",  -field );
  #else
  Edge      usedge( "",   tan(arg.getEntryAngle())*field );
  Edge      dsedge( "",  -tan(arg.getExitAngle())*field );
  #endif

  sbend     usbend( "" ,  frontLength,     field, (frontLength/arg.Length())*arg.getBendAngle(), arg.getEntryFaceAngle(), 0.0                ); 
  sbend     dsbend( "",   frontLength,     field, (frontLength/arg.Length())*arg.getBendAngle(), 0.0,                 arg.getExitFaceAngle() );

  sbend  separator( "",   frontLength,     field, (frontLength/arg.Length())*arg.getBendAngle(), 0.0, 0.0 );
  sbend       body( "",   sepLength,       field, (  sepLength/arg.Length())*arg.getBendAngle(), 0.0, 0.0 );

  usbend.nullEntryEdge();
  usbend.nullExitEdge();
  dsbend.nullEntryEdge();
  dsbend.nullExitEdge();
  separator.nullEntryEdge();
  separator.nullExitEdge();
  body.nullEntryEdge();
  body.nullExitEdge();

  InducedKick fqk("", 0.0, bend_radius);
  InducedSextupoleKick fsk("", 0.0, bend_radius);

  bml = BmlPtr( new beamline("CF_SBEND_INTERNALS") );

  for( int i=0; i<n_; ++i) {

    if ( i == 0 ) { 
      bml->append( usedge );
      bml->append( usbend );
    } 
    else {
      bml->append( separator);
    }

    bml->append( fqk      );
    bml->append( fsk      );
    bml->append( body     );
    bml->append( fqk      );
    bml->append( fsk      );
    bml->append( body     );
    bml->append( fqk      );
    bml->append( fsk      );
    bml->append( body     );
    bml->append( fqk      );
    bml->append( fsk      );

    if ( i == n_-1 ) { 
      bml->append( dsbend );
      bml->append( dsedge );
    } 
    else {
      bml->append( separator );
    }
  }

  arg.setQuadrupole( quadStrength );
  arg.setSextupole ( sextStrength );

  if( hasOldBml ) 
  {
    if( !hasEntryEdge ) { arg.nullEntryEdge(); }
    if( !hasExitEdge  ) { arg.nullExitEdge();  }
  }
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

template <typename Particle_t>
void CF_sbend::Propagator::propagateFirst( CF_sbend& elm, Particle_t&     p )
{

    typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

    State_t& state = p.State();
    BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( elm );

    // find exit edge element
    if (bml->howMany() == 0)
        throw std::runtime_error("CF_SBendPropagators::propagateFirst empty internal beamline");

    bool hasExitEdge = ( typeid(*(bml->lastElement()))  == typeid(Edge) );
    beamline::iterator bit_end   = bml->end();

    // dont propagate the last element (should be an edge)
    if (hasExitEdge) --bit_end;

    ::propagateSome(bml->begin(), bit_end, p);

    state[i_cdt] -= elm.getReferenceTime();
}

template void CF_sbend::Propagator::propagateFirst<Particle>( CF_sbend& elm, Particle&     p );
template void CF_sbend::Propagator::propagateFirst<JetParticle>( CF_sbend& elm, JetParticle&     p );

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// particles enter in the local frame and leave in the global frame
template <typename Particle_t>
void CF_sbend::Propagator::propagateLast( CF_sbend& elm, Particle_t&     p )
{


    BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( elm );

    // find exit edge element
    if (bml->howMany() == 0)
        throw std::runtime_error("CF_SBendPropagators::propagateFirst empty internal beamline");

    bool hasExitEdge = ( typeid(*(bml->lastElement()))  == typeid(Edge) );

    if (hasExitEdge)
    {
        beamline::iterator bit_start = bml->end();
        --bit_start;
        ::propagateSome(bit_start, bml->end(), p);
    }

    //state[i_cdt] -= elm.getReferenceTime();
}
 
template void CF_sbend::Propagator::propagateLast<Particle>( CF_sbend& elm, Particle&     p );
template void CF_sbend::Propagator::propagateLast<JetParticle>( CF_sbend& elm, JetParticle&     p );
 
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void CF_sbend::Propagator::operator()(CF_sbend& elm, Particle& p )
{ 
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void CF_sbend::Propagator::operator()(CF_sbend& elm, JetParticle& p )
{ 
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
