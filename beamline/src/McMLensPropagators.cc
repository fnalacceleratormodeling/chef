/*************************************************************************
**************************************************************************
**************************************************************************
******
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and
******             synchrotrons.
******
******  File:      McMLensPropagators.cc
******
******  Copyright Fermi Research Alliance / Fermilab
******            All Rights Reserved
*****
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contract No. DE-AC02-07CH11359
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******
******  Author:    Eric G. Stern
******             Email: egstern@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******
******  Nov 2020            egstern@fnal.gov
******  - implement cylindrical McMillan lens
******
**************************************************************************
*************************************************************************/

#include <basic_toolkit/PhysicsConstants.h>
#include <beamline/McMLensPropagators.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/ParticleBunch.h>
#include <beamline/McMLens.h>

using namespace std;
using FNAL::pcerr;
using FNAL::pcout;

namespace {

  Particle::PhaseSpaceIndex const& i_x   = Particle::xIndex;
  Particle::PhaseSpaceIndex const& i_y   = Particle::yIndex;
  Particle::PhaseSpaceIndex const& i_cdt = Particle::cdtIndex;
  Particle::PhaseSpaceIndex const& i_npx = Particle::npxIndex;
  Particle::PhaseSpaceIndex const& i_npy = Particle::npyIndex;

  template<typename Particle_t>
  void driftpropagate( double length, bmlnElmnt& elm, Particle_t& p )
  {
       typedef typename PropagatorTraits<Particle_t>::State_t       State_t;
       typedef typename PropagatorTraits<Particle_t>::Component_t   Component_t;

       State_t& state = p.State();

       Component_t npz = p.get_npz();

       Component_t xpr = state[i_npx] / npz;
       Component_t ypr = state[i_npy] / npz;


       state[i_x] += length* xpr;
       state[i_y] += length* ypr;

       state[i_cdt] += length*sqrt( 1.0 + xpr*xpr + ypr*ypr )/ p.Beta();
  }

  /*
 !*****************************************************************
    ! The following subroutine computes the nonlinear momentum kick
    ! across a thin lens circular McMillan lens as described in
    ! I. Lobach, et al., MCMILLAN LENS IN A SYSTEM WITH SPACE CHARGE
    !     IPAC2018, Vancouver, BC, Canada, doi: 10.18429/JACoW-IPAC2018-THPAF071
    !
    !  \Delta p_r =                 -k_m \vec{r}
    !                         -----------------------
    !                                   r^2
    !                           1  +   -----
    !                                   r_m^2
    !
    !
    !                     ( 1 \pm \beta_z \beta_e )
    !     k_m =  j0 L  -----------------------------------------
    !                   2 ( B \rho ) \epsilon_0 \beta_z \beta_e
    !
    !*****************************************************************
*/

template<typename Particle_t>
void applyKick( McMLens& elm, Particle_t& p )
{
  typedef typename PropagatorTraits<Particle_t>::State_t       State_t;
  typedef typename PropagatorTraits<Particle_t>::Component_t   Component_t;

  State_t& state = p.State();
#if 1
  double const j0 = elm.Strength();
  // if length == 0 then Strength is j0*L.
  double const length = elm.Length();
  double const j0L = ((length == 0.0) ? (j0) : (j0*length) );
  double const beta_e = elm.get_beta_e();
  double const rm = elm.get_rm();
  double const rmsq = rm*rm;
  Component_t beta_z = sqrt(1.0 - state[i_npx]*state[i_npx] - state[i_npy]*state[i_npy])*p.ReferenceBeta();
  Component_t r_squared = state[i_x]*state[i_x] + state[i_y]*state[i_y];

  Component_t Kr = -j0L *
                      ((1.0 - beta_e*beta_z)/(2.0 * p.BRho() * PH_MKS_eps0 * beta_e * beta_z * PH_MKS_c*PH_MKS_c)) *
                      (1.0/(1.0 + r_squared/rmsq));
#else
  Component_t Kr = -0.2;
#endif
  Component_t dPx = Kr * state[i_x];
  Component_t dPy = Kr * state[i_y];
  state[i_npx] += dPx;
  state[i_npy] += dPy;
}

template<typename Element_t, typename Particle_t>
void propagate( Element_t& elm, Particle_t& p )
{
  typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

  State_t& state = p.State();

  if (elm.Length() > 0.0 ) {

    ::driftpropagate( elm.Length()/2, elm, p );   // Drift through first  half of the length
    applyKick(elm,p);
    ::driftpropagate( elm.Length()/2, elm, p );   // Drift through second half of the length.

    state[i_cdt] -= elm.getReferenceTime();
 }
  else {
    applyKick(elm,p);
  }

 }

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------

#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void propagate(     McMLens& elm,    Particle& p );
template void propagate(     McMLens& elm, JetParticle& p );

#endif

} // namespace

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void McMLens::Propagator::setup( McMLens& elm )
{}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void McMLens::Propagator::operator()( McMLens& elm, Particle& p )
{
  ::propagate(elm ,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void McMLens::Propagator::operator()( McMLens& elm, JetParticle& p )
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

namespace {
} // anonymous namespace
