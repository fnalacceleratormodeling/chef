/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      InducedSextupoleKickPropagators.cc
******                                                                
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Author:    Eric Stern    egstern@fnal.gov
******             Qiming Lu     qlu@fnal.gov
******                                                                
******                                                                
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Sep 2017           qlu@fnal.gov
******  - initial version.
******  - implementation of propagator for class InducedSextupoleKick,
******    a magnetic kick to be used in CF_sbend magnets.
******  - uses expressions for induced, effective magnetic fields 
******    for cylindrical geometry, derived independently
******    by Timofey Zolkin (2015) and Edwin McMillan (1975).
******  - implemented based on the InducedKickPropagator class
******                                                                
**************************************************************************
*************************************************************************/


#include <basic_toolkit/iosetup.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/InducedSextupoleKick.h>
#include <beamline/InducedSextupoleKickPropagators.h>

using namespace std;
using FNAL::pcerr;
using FNAL::pcout;

namespace {

  Particle::PhaseSpaceIndex i_x   = Particle::xIndex;
  Particle::PhaseSpaceIndex i_y   = Particle::yIndex;
  Particle::PhaseSpaceIndex i_npx = Particle::npxIndex;
  Particle::PhaseSpaceIndex i_npy = Particle::npyIndex;



template<typename Particle_t>
void applyKick( InducedSextupoleKick& elm, Particle_t& p)
{
    typedef typename PropagatorTraits<Particle_t>::State_t       State_t;
    typedef typename PropagatorTraits<Particle_t>::Component_t   Component_t;

    double const hk =  elm.Strength() / p.ReferenceBRho();
    double const R0 = elm.get_R0();

    State_t& state = p.State();

    Component_t alf = state[i_x]/R0;
    Component_t x   = state[i_x];
    Component_t y   = state[i_y];

// model 10 is the full Zolkin expression for the magnetic field
// model 11 uses the expansion of the Zolkin field to x**3
// model 12 uses the expansion of the Zolkin field to x**2
// model 13 uses the linear expansion of the field and happens to match PTC for the small lattice
#define MODEL 10
#if MODEL==10
    Component_t Bx = x * (2.0 + alf) * y / (1.0 + alf);
    Component_t By = 0.5 * (x * x + 2.0 * x * R0) - y * y - R0 * R0 * log(1.0 + alf);
    state[i_npx] += -hk * By * (1.0 + alf);
    state[i_npy] +=  hk * Bx * (1.0 + alf);
#elif MODEL==11
#elif MODEL==12
#elif MODEL==13
#endif

}


template<typename Element_t, typename Particle_t>
void propagate( Element_t& elm, Particle_t& p )
{

    applyKick(elm,p);
 
 }

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------
#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void driftpropagate( double length, bmlnElmnt& elm, Particle& p );
template void driftpropagate( double length, bmlnElmnt& elm, JetParticle& p );

template void applyKick( InducedSextupoleKick& elm, Particle& p);
template void applyKick( InducedSextupoleKick& elm, JetParticle& p);

template void propagate( InducedSextupoleKick& elm,    Particle& p );
template void propagate( InducedSextupoleKick& elm, JetParticle& p );

#endif
//-----------------------------------------------------------------------------------


} // anonymous namespace

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::Propagator::setup( InducedSextupoleKick& elm ) 
{}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::Propagator::operator()( InducedSextupoleKick& elm, Particle& p ) 
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::Propagator::operator()( InducedSextupoleKick& elm, JetParticle& p ) 
{
  ::propagate(elm,p);
}
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

