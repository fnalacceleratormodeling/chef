/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      EdgePropagators.tcc
******                                                                
******  Copyright Fermi Research Alliance / Fermilab    
******            All Rights Reserved                             
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-07CH11359 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws.
******                                                                
******                                                                
******             Jean-Francois Ostiguy  ostiguy@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******  
******  Apr 2015            michelotti@fnal.gov
******  - added option of using dynamically calculated entry and exit
******    angles in the edge propagators, instead of the angles 
******    "hard-wired" by RefRegVisitor. To enable this option,
******    pass a -DNO_FIXED_ENTRY_ANGLE macro definition to the compiler.
******    (See update below.)
******
******  Aug 2016            michelotti@fnal.gov
******  - NO_FIXED_ENTRY_ANGLE is now defined as true in the header
******    file Edge.h. The previous default behavior is changed. Getting
******    it back now is not a compile time option but must be done
******    by rewriting the line in the header file before compilation.
******    For further explanation, read comments added to Edge.h.
******
******  Mar 2017            michelotti@fnal.gov
******  - the NO_FIXED_ENTRY_ANGLE option is "fix"ed by forcing a
******  symplectic kick even for non-standard Jet environments.
******  This trick is a bit fraudulent, but not much more so than
******  treating an edge as a thin element kick to begin with.
******  - there may be an (untested, presumably small?) effect on
******  closed orbit calculations.
******
**************************************************************************
*************************************************************************/

#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/beamline.h>
#include <beamline/Edge.h>
#include <beamline/EdgePropagators.h>
#include <beamline/drift.h>
#include <iostream>

namespace {

  Particle::PhaseSpaceIndex i_y   = Particle::yIndex;
  Particle::PhaseSpaceIndex i_npy = Particle::npyIndex;

template <typename Particle_t>
void propagate( Edge& elm, Particle_t & p ) 
{

 // "Strength" is B'l in Tesla

 typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

 State_t& state = p.State();

 if( elm.Strength() == 0.0 ) return; 
 
 #if NO_FIXED_ENTRY_ANGLE
 // --------------------------
 Component_t npx_in = p.get_npx();
 Component_t npy_in = p.get_npy();
 Component_t npz_in = p.get_npz();

 double k = ( p.Charge() > 0.0 ) ? (  (fraud_value(npx_in)/fraud_value(npz_in))*elm.Strength() / p.ReferenceBRho() ) 
                                 : ( -(fraud_value(npx_in)/fraud_value(npz_in))*elm.Strength() / p.ReferenceBRho() ) ;
 state[i_npy] -=  k * state[i_y];

        k = ( p.Charge() > 0.0 ) ? (  (fraud_value(npy_in)/fraud_value(npz_in))*elm.Strength() / p.ReferenceBRho() )
                                 : ( -(fraud_value(npy_in)/fraud_value(npz_in))*elm.Strength() / p.ReferenceBRho() ) ;
 state[i_npx] +=  k * state[i_y];
 // --------------------------
 #else
 // --------------------------
 double const k = ( p.Charge() > 0.0 ) ? ( elm.Strength() / p.ReferenceBRho()) : (-elm.Strength() / p.ReferenceBRho() ) ;
 state[i_npy] -=  k * state[i_y];
 // --------------------------
 #endif

}

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------
#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void propagate( Edge& elm,    Particle& p );
template void propagate( Edge& elm, JetParticle& p );

#endif
//-----------------------------------------------------------------------------------

} // namespace

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void Edge::Propagator::operator()( Edge& elm, Particle& p ) 
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void Edge::Propagator::operator()( Edge& elm, JetParticle&     p ) 
{
  ::propagate(elm,p);

}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

