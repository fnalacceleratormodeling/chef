/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******
******  File:      sector.cc
******                                                                
******  Copyright (c) Fermi Research Alliance
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******
******  Original author: Leo Michelotti  (michelotti@fnal.gov)
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Mar 2007         Jean-Francois Ostiguy  (ostiguy@fnal.gov)
******  - support for reference counted elements
******  - reduced src file coupling due to visitor interface. 
******    visit() takes advantage of (reference) dynamic type.
******  - use std::string for string operations.
******
******  Jul 2007         ostiguy@fnal.gov
******  - restructured constructors   
******
******  Dec 2007         ostiguy@fnal.gov
******  - new typesafe propagators
******
******  Aug 2015         michelotti@fnal.gov
******  - added constructor with a matrix argument
******
******  Oct 2015         michelotti@fnal.gov
******  - added ability to split a sector under limited circumstances.
******    This creates radically non-physically motivated objects.
******
**************************************************************************
*************************************************************************/

#include <basic_toolkit/iosetup.h>
#include <basic_toolkit/GenericException.h>
#include <beamline/sector.h>
#include <beamline/SectorPropagators.h>
#include <beamline/BmlVisitor.h>
#include <beamline/marker.h>
#include <beamline/Alignment.h>

using namespace std;
using FNAL::pcerr;
using FNAL::pcout;

namespace {
  int const  BMLN_dynDim = 6;
}

// **************************************************
//   class sector
// **************************************************

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

sector::sector( const char* n, std::vector<double> const& bH,  std::vector<double> const& aH,  std::vector<double> const& pH, 
                               std::vector<double> const& bV,  std::vector<double> const& aV,  std::vector<double> const& pV,
                               double bL, double psiL, double l  )
: bmlnElmnt( n, l ),
  mapType_(0),
  can_split_(false),
  myMap_(),
  betaH_(bH),     // 0 = entry;  1 = exit
  alphaH_(aH),    
  deltaPsiH_(0.0),
  betaV_(bV),     
  alphaV_(aV),    
  deltaPsiV_(0.0),
  betaL_(bL),
  psiL_(psiL),
  mapMatrix_(BMLN_dynDim,BMLN_dynDim)
{
 if( pH[1] <= pH[0] ) {
   throw( GenericException( __FILE__, __LINE__, 
          "sector::sector( ...)"
          "Horizontal phases inverted." ) );
 }
 else deltaPsiH_ = pH[1] - pH[0];

 if( pV[1] <= pV[0] ) {
   throw( GenericException( __FILE__, __LINE__, 
          "sector::sector( ...)", 
          "Vertical phases inverted." ) );
 }
 else deltaPsiV_ = pV[1] - pV[0];

 for   ( int i=0; i< BMLN_dynDim; ++i) {
     mapMatrix_(i,i) = 1.0;
 }

 double    dummy = sqrt( betaH_[1] / betaH_[0] );   // --- Horizontal sector
 double       cs = cos( deltaPsiH_ );
 double       sn = sin( deltaPsiH_ );
 mapMatrix_(0,0) = ( cs + alphaH_[0] * sn ) * dummy;
 mapMatrix_(3,3) = ( cs - alphaH_[1] * sn ) / dummy;
 dummy           = sqrt( betaH_[0] * betaH_[1] );
 mapMatrix_(0,3) = dummy * sn;
 mapMatrix_(3,0) = (   ( alphaH_[0] - alphaH_[1]     ) * cs
                     - ( 1.0 + alphaH_[0]*alphaH_[1] ) * sn
                   ) / dummy;

           dummy = sqrt( betaV_[1] / betaV_[0] );   // --- Vertical sector
              cs = cos( deltaPsiV_ );
              sn = sin( deltaPsiV_ );
 mapMatrix_(1,1) = ( cs + alphaV_[0] * sn ) * dummy;
 mapMatrix_(4,4) = ( cs - alphaV_[1] * sn ) / dummy;
           dummy = sqrt( betaV_[0] * betaV_[1] );
 mapMatrix_(1,4) = dummy * sn;
 mapMatrix_(4,1) = (   ( alphaV_[0] - alphaV_[1]     ) * cs
                     - ( 1.0 + alphaV_[0]*alphaV_[1] ) * sn
                   ) / dummy;

 // longitudinal sector is like transverse with contant beta and
 // alpha = 0.
 mapMatrix_(2, 2) = cos(psiL);
 mapMatrix_(5, 5) = cos(psiL);
 mapMatrix_(2, 5) = bL*sin(psiL);
 mapMatrix_(5, 2) = -sin(psiL)/bL;
 
 can_split_ = true;

 propagator_ = PropagatorPtr( new Propagator() );
 propagator_->setup(*this);

} // end function sector::sector( std::vector<double>& bH, ... )


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

sector::sector( const char* n, Mapping const& m, double l, char mpt )
: bmlnElmnt( n, l, 0.0), 
  mapType_(mpt),        
  can_split_(false),
  myMap_(m),
  betaH_(),     
  alphaH_(),    
  deltaPsiH_(0.0),
  betaV_(),     
  alphaV_(),    
  deltaPsiV_(0.0),
  betaL_(1.0),
  psiL_(0.0),
  mapMatrix_()
{
 propagator_ = PropagatorPtr( new Propagator() );
 propagator_->setup(*this);
 if( mpt == 0 ) { mapMatrix_ = myMap_.Jacobian(); } 
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

sector::sector( const char* nm, MatrixD const& transmat, double lng )
: bmlnElmnt( nm, lng, 0.0), 
  mapType_(0),
  can_split_(false),
  myMap_(),
  betaH_(),     
  alphaH_(),    
  deltaPsiH_(0.0),
  betaV_(),     
  alphaV_(),    
  deltaPsiV_(0.0),
  betaL_(1.0),
  psiL_(0.0),
  mapMatrix_(transmat)
{
 propagator_ = PropagatorPtr( new Propagator() );
 propagator_->setup(*this);
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

sector::sector( const char* n, double l, char mpt )
: bmlnElmnt( n, l, 0.0), 
  mapType_(mpt),        
  can_split_(false),
  myMap_(),
  betaH_(),     
  alphaH_(),    
  deltaPsiH_(0.0),
  betaV_(),     
  alphaV_(),    
  deltaPsiV_(0.0),
  betaL_(1.0),
  psiL_(0.0),
  mapMatrix_( )
{
 propagator_ = PropagatorPtr( new Propagator() );
 propagator_->setup(*this);

 if( mpt == 0 ) { mapMatrix_ = myMap_.Jacobian(); } 
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

sector::sector( sector const& x )
: bmlnElmnt(x),
  mapType_(x.mapType_),        
  can_split_(x.can_split_),
  myMap_(x.myMap_),
  betaH_(x.betaH_),     
  alphaH_(x.alphaH_),    
  deltaPsiH_(x.deltaPsiH_),
  betaV_(x.betaV_),     
  alphaV_(x.alphaV_),    
  deltaPsiV_(x.deltaPsiV_),
  betaL_(x.betaL_),
  psiL_(x.psiL_),
  mapMatrix_(x.mapMatrix_),
  propagator_(x.propagator_->Clone() )
{}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

sector::~sector() 
{}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void sector::Split( double pc, ElmPtr& a, ElmPtr& b ) const
{
  if( !can_split_ ) {
    (*pcerr) <<   "*** WARNING ****: "
                "\n*** WARNING ****: "  << __FILE__ << "," << __LINE__
             << "\n*** WARNING ****: void sector::Split( double, ElmPtr&, ElmPtr& ) const"
                "\n*** WARNING ****: Splitting a sector that uses a Mapping is forbidden."
                "\n*** WARNING ****: " 
             << std::endl;
    ostringstream uic;
    uic  <<   "Splitting a " << Type() << " is forbidden in this version.";
    throw( GenericException( __FILE__, __LINE__, 
           "void sector::Split( double, ElmPtr&, ElmPtr& ) const",
           uic.str().c_str() ) );
  }

  if( ( pc <= 0.0 ) || ( pc >= 1.0 ) ) {
    ostringstream uic;
    uic  << "pc = " << pc << ": this should be within (0,1).";
    throw( GenericException( __FILE__, __LINE__, 
           "void sector::Split( double pc, bmlnElmnt** a, bmlnElmnt** b )",
           uic.str().c_str() ) );
  }

  std::vector<double> betaH(2);
  std::vector<double> alphaH(2);
  std::vector<double> psiH(2);
  std::vector<double> betaV(2);
  std::vector<double> alphaV(2);
  std::vector<double> psiV(2);
  double betaL;
  double length;

  // Construct upstream piece
  // ------------------------

  length = pc*length_;
  betaL = betaL_;

  betaH[0]  = betaH_[0];
  betaH[1]  = (1.0 - pc)*betaH_[0]  + pc*betaH_[1];
  alphaH[0] = alphaH_[0];
  alphaH[1] = (1.0 - pc)*alphaH_[0] + pc*alphaH_[1];
  psiH[0]   = 0.0;
  psiH[1]   = pc*deltaPsiH_;

  betaV[0]  = betaV_[0];
  betaV[1]  = (1.0 - pc)*betaV_[0]  + pc*betaV_[1];
  alphaV[0] = alphaV_[0];
  alphaV[1] = (1.0 - pc)*alphaV_[0] + pc*alphaV_[1];
  psiV[0]   = 0.0;
  psiV[1]   = pc*deltaPsiV_;

  a = SectorPtr( new sector( "", betaH, alphaH, psiH, betaV, alphaV, psiV, betaL, pc*psiL_, length ) );

  // Construct downstream piece
  // --------------------------

  length = length_ - length;

  betaH[0]  = betaH[1];
  betaH[1]  = betaH_[1];
  alphaH[0] = alphaH[1];
  alphaH[1] = alphaH_[1];
  psiH[0]   = psiH[1];
  psiH[1]   = deltaPsiH_;

  betaV[0]  = betaV[1];
  betaV[1]  = betaV_[1];
  alphaV[0] = alphaV[1];
  alphaV[1] = alphaV_[1];
  psiV[0]   = psiV[1];
  psiV[1]   = deltaPsiV_;

  b = SectorPtr( new sector( "", betaH, alphaH, psiH, betaV, alphaV, psiV, betaL, (1.0-pc)*psiL_, length ) );

  // Set the alignment struct
  // : this is a STOPGAP MEASURE!!!
  // -----------------------------------------------------------------
  a->setAlignment( Alignment() );
  b->setAlignment( Alignment() );

  // Rename
  a->rename( ident_ + string("_1") );
  b->rename( ident_ + string("_2") );
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

Mapping const& sector::getMap() const
{
  return myMap_;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

Matrix sector::getMatrix() const
{
  return ( mapType_ == 0) ? mapMatrix_ : myMap_.Jacobian();
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void sector::setFrequency( double (*fcn)( double ) )
{
  DeltaT = fcn;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void sector::setFrequency( Jet (*fcn)( const Jet& ) ) 
{
  JetDeltaT = fcn;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

const char* sector::Type() const 
{ 
  return "sector"; 
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void sector::setLength( double )
{
  (*pcerr) << "*** WARNING ***                                \n"
          "*** WARNING *** sector::setLength does nothing.\n"
          "*** WARNING ***                                \n"
       << endl;
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

ostream& sector::writeTo ( ostream& os )
{
  if( mapType_ == 0 )
  {
    os << myMap_;
  }
  else 
  {
    (*pcerr) << "*** ERROR ***                                    \n"
         << "*** ERROR *** sector::writeTo                    \n"
         << "*** ERROR *** This is written only to handle     \n"
         << "*** ERROR *** mapType = 0.                       \n"
         << "*** ERROR ***                                    \n"
         << "*** ERROR *** The sector is not streamed.        \n"
         << "*** ERROR ***                                    \n"
         << endl;
  }
  
  return os;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

istream& sector::readFrom( istream& is )
{
  mapType_ = 0;
  is >> myMap_;
  return is;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

bool  sector::isMatrix() const 
{
  return ( mapType_ == 0 );
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

bool  sector::isMagnet() const 
{
  return false;  
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  sector::accept( BmlVisitor& v )
{
  v.visit(*this);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  sector::accept( ConstBmlVisitor& v ) const
{
  v.visit(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void sector::usePropagator( PropagatorPtr& x )
{
  propagator_ = PropagatorPtr( x->Clone() );
  propagator_->setup( *this );
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  sector::localPropagate( Particle& p ) 
{
  (*propagator_)(*this, p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  sector::localPropagate( JetParticle& p ) 
{
  (*propagator_)(*this, p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  sector::localPropagate( ParticleBunch& b ) 
{
  (*propagator_)(*this, b);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void  sector::localPropagate( JetParticleBunch& b ) 
{
  (*propagator_)(*this, b);
}



