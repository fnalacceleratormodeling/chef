/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      SectorPropagators.cc
******                                                                
******  Copyright Universities Research Association, Inc./ Fermilab    
******            All Rights Reserved                             
*****
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws.
******                                                                
******  Author:    Leo Michelotti                                     
******                                                                
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 220                                      
******             Batavia, IL   60510                                
******                                                                
******             Phone: (630) 840 4956                              
******             Email: michelotti@fnal.gov  
******
****** REVISION HISTORY
******
****** July 2007  ostiguy@fnal.gov
******                            
****** - refactored and streamlined code: take advantage of 
******   Vector and Mapping operators.  
******                                    
******
**************************************************************************
*************************************************************************/


#include <beamline/SectorPropagators.h>
#include <beamline/sector.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>

namespace {

template<typename Particle_t>
void propagate( sector& elm, Particle_t& p )
{

 typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

 State_t& state = p.State();

 if ( elm.isMatrix() ) { 
  state =  elm.getMatrix()   * state; 
 } 
 else {
  state =  elm.getMap()      * state; // composition
 }
}

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------

#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void propagate(     sector& elm,    Particle& p );
template void propagate(     sector& elm, JetParticle& p );

#endif

} // anonymous namespace 

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void sector::Propagator::setup(sector& elm )
{}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void sector::Propagator::operator()( sector& elm, Particle& p ) 
{
  ::propagate(elm,p);
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void sector::Propagator::operator()( sector& elm, JetParticle& p ) 
{
  ::propagate(elm,p);
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

