/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      RbendPropagators.tcc
******                                                                
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******
******  Authors:   Leo Michelotti         michelotti@fnal.gov
******             Jean-Francois Ostiguy  ostiguy@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******  
******  Jan 2015            michelotti@fnal.gov
******  - bug fix: added code to the .setup routine for reinitializing
******    a pre-existing rbend with fewer than two edge elements.
******    : as a reminder, the issue of multiply redundant setups
******      has never been handled satisfactorily.
******  
******  Apr 2015            michelotti@fnal.gov
******  - added option of using dynamically calculated entry and exit
******    angles in the edge propagators, instead of the angles 
******    "hard-wired" by RefRegVisitor. To enable this option,
******    pass a -DNO_FIXED_ENTRY_ANGLE macro definition to the compiler.
******    (See update below.)
******
******  Aug 2016            michelotti@fnal.gov
******  - NO_FIXED_ENTRY_ANGLE is now defined as true in the header
******    file Edge.h. The previous default behavior is changed. Getting
******    it back now is not a compile time option but must be done
******    by rewriting the line in the header file before compilation.
******    For further explanation, read comments added to Edge.h.
******
**************************************************************************
*************************************************************************/

#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/beamline.h>
#include <beamline/RBendPropagators.h>
#include <beamline/Edge.h>
#include <beamline/Bend.h>
#include <beamline/rbend.h>
#include <beamline/drift.h>
#include <iostream>

namespace {

  Particle::PhaseSpaceIndex i_cdt = Particle::cdtIndex;

template<typename Particle_t>
void propagateSome( beamline::iterator bit_start, beamline::iterator bit_end, Particle_t &p)
{

    for (beamline::iterator it = bit_start; it != bit_end; ++it) {
        (*it)->localPropagate(p);
    }
}
 
template<typename Particle_t>
void propagate( rbend& elm, Particle_t& p )
{
   
  typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

  State_t& state = p.State();

  BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( elm );

  propagateSome(bml->begin(), bml->end(), p);

 state[i_cdt] -= elm.getReferenceTime();  

}

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------

#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void propagate(     rbend& elm,    Particle& p );
template void propagate(     rbend& elm, JetParticle& p );

#endif

} // namespace

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rbend::Propagator::setup( rbend& arg) 
{
  bool hasEntryEdge = false;
  bool hasExitEdge  = false;
  bool hasOldBml    = false;

  BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( arg); 

  if( bml ) {
    hasOldBml    = true;
    hasEntryEdge = ( typeid(*(bml->firstElement())) == typeid(Edge) );
    hasExitEdge  = ( typeid(*(bml->lastElement()))  == typeid(Edge) );
  }

  double& usFaceAngle_ = rbend::rbend_core_access::get_usFaceAngle(arg); 
  double& dsFaceAngle_ = rbend::rbend_core_access::get_dsFaceAngle(arg); 
  double& usAngle_     = rbend::rbend_core_access::get_usAngle(arg); 
  double& dsAngle_     = rbend::rbend_core_access::get_dsAngle(arg); 

  #if NO_FIXED_ENTRY_ANGLE
  EdgePtr uedge( new Edge( "",  arg.Strength() ) );
  EdgePtr dedge( new Edge( "", -arg.Strength() ) );
  #else
  EdgePtr uedge( new Edge( "",  tan(usAngle_) * arg.Strength() ) );
  EdgePtr dedge( new Edge( "", -tan(dsAngle_) * arg.Strength() ) );
  #endif

  BendPtr bend ( new Bend( "",  arg.Length(),   arg.Strength(), arg.getBendAngle(),
                                usAngle_,  dsAngle_, usFaceAngle_,  dsFaceAngle_,  // ??? Bends should not need
                                Bend::type_rbend ) );                              // ??? usAngle_ and dsAngle_

  bml = BmlPtr(new beamline("RBEND_PRIVATE") );
  bml->append( uedge );
  bml->append( bend  );
  bml->append( dedge );

  if( hasOldBml ) 
  {
    if( !hasEntryEdge ) { arg.nullEntryEdge(); }
    if( !hasExitEdge  ) { arg.nullExitEdge();  }
  }
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rbend::Propagator::operator()( rbend& elm, Particle& p ) 
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void rbend::Propagator::operator()( rbend& elm, JetParticle& p ) 
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
 
template <typename Particle_t>
void rbend::Propagator::propagateFirst( rbend& elm, Particle_t&     p )
{

    typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

    State_t& state = p.State();
    BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( elm );

    // find exit edge element
    if (bml->howMany() == 0)
        throw std::runtime_error("RBendPropagators::propagateFirst empty internal beamline");

    bool hasExitEdge = ( typeid(*(bml->lastElement()))  == typeid(Edge) );
    beamline::iterator bit_end = bml->end();

    // dont propagate the last element (should be an edge)
    if (hasExitEdge) --bit_end;

    ::propagateSome(bml->begin(), bit_end, p);

    state[i_cdt] -= elm.getReferenceTime();
}

template void rbend::Propagator::propagateFirst<Particle>( rbend& elm, Particle&     p );
template void rbend::Propagator::propagateFirst<JetParticle>( rbend& elm, JetParticle&     p );

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// particles enter in the local frame and leave in the global frame
template <typename Particle_t>
void rbend::Propagator::propagateLast( rbend& elm, Particle_t&     p )
{


    BmlPtr& bml = bmlnElmnt::core_access::get_BmlPtr( elm );

    // find exit edge element
    if (bml->howMany() == 0)
        throw std::runtime_error("RBendPropagators::propagateFirst empty internal beamline");

    bool hasExitEdge = ( typeid(*(bml->lastElement()))  == typeid(Edge) );

    if (hasExitEdge)
    {
        beamline::iterator bit_start = bml->end();
        --bit_start;
        ::propagateSome(bit_start, bml->end(), p);
    }

    //state[i_cdt] -= elm.getReferenceTime();
}
 
template void rbend::Propagator::propagateLast<Particle>( rbend& elm, Particle&     p );
template void rbend::Propagator::propagateLast<JetParticle>( rbend& elm, JetParticle&     p );
 

