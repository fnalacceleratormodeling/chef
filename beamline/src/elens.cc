/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      elens.cc
******                                                                
******  Copyright Universities Research Association, Inc./ Fermilab    
******            All Rights Reserved                             
******  
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws.
******                                                                
******  Author:    Eric G. Stern
******                                                                
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 220                                      
******             Batavia, IL   60510                                
******                                                                
******             Phone: (630) 840 4747
******             Email: egstern@fnal.gov
******
****** REVISION HISTORY
******/

#include <iomanip>
#include <beamline/elens.h>
#include <beamline/ElensPropagators.h>
#include <beamline/BmlVisitor.h>
#include <beamline/Alignment.h>

using namespace std;

// **************************************************
//   class elens
// **************************************************

#if 0
elens::elens() // this defined a useless element
: bmlnElmnt(), eenergy(0.0), radius(0.0), profile(undefined)
{
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}
#endif

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#if 0  // why would we even want this signature?
elens::elens( const char* n )
  : bmlnElmnt(n, 0.0, 0.0), radius(0.0), eenergy(eenergy), profile(undefined)
{   
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}
#endif
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

elens::elens( const char* name,
              double length, double current,
              double eenergy, double radius,
              double longrms, e_profile_t const& profile )
  : bmlnElmnt(name, length, current), eenergy(eenergy), radius(radius),
    longrms(longrms), profile(profile)
{ 
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

elens::elens( elens const& x )
  : bmlnElmnt( x ), eenergy(x.eenergy), radius(x.radius),
    longrms( x.longrms), profile(x.profile),
    propagator_(x.propagator_->Clone() )
{}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#if 0
elens::elens( const char* n, double l, double s )
: bmlnElmnt(n,l,s) 
{
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}
#endif

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

elens&  elens::operator=( elens const& rhs) {

  if ( &rhs == this ) return *this;  
  bmlnElmnt::operator=(rhs);

  propagator_ = PropagatorPtr( rhs.propagator_->Clone() );

  return *this; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

elens::~elens()
{}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

const char* elens::Type() const
{ 
  return "elens";
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

bool elens::isMagnet() const
{ 
  return false;
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::Split( double pc, ElmPtr& a, ElmPtr& b ) const
{
  if( ( pc <= 0.0 ) || ( pc >= 1.0 ) ) {
    ostringstream uic;
    uic  << "pc = " << pc << ": this should be within [0,1].";
    throw( GenericException( __FILE__, __LINE__, 
           "void elens::Split( double pc, bmlnElmnt** a, bmlnElmnt** b )",
           uic.str().c_str() ) );
  }

  a = ElensPtr( Clone() );
  b = ElensPtr( Clone() );

  a->setLength( pc        * length_ );
  b->setLength( (1.0- pc) * length_ );

  // We assume "strength" field*length_.  This is not normal,
  // but the kicks are non-physical, fictitious elements.
  // It is because of this that these lines are needed
  // and using the default bmlnElmnt::Split method
  // produces a wrong result.
  // ------------------------
  a->setStrength( pc        * strength_ );
  b->setStrength( (1.0- pc) * strength_ );

  // Set the alignment struct
  // : this is a STOPGAP MEASURE!!!
  // ------------------------------
  a->setAlignment( Alignment() );
  b->setAlignment( Alignment() );

  // Rename
  a->rename( ident_ + string("_1") );
  b->rename( ident_ + string("_2") );
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::accept( BmlVisitor& v )
{ 
  v.visit( *this ); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::accept( ConstBmlVisitor& v ) const
{
   v.visit( *this ); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void elens::usePropagator( PropagatorPtr& x )
{
  propagator_ = PropagatorPtr( x->Clone() );
  propagator_->setup( *this );
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::localPropagate( Particle& p)
{ 
  (*propagator_)(*this,p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::localPropagate( JetParticle& p)
{ 
  (*propagator_)(*this,p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::localPropagate( ParticleBunch& b)
{ 
  (*propagator_)(*this,b);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::localPropagate( JetParticleBunch& b)
{ 
  (*propagator_)(*this,b);
}
