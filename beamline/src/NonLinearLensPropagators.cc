/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      NonLinearLensPropagators.cc
******                                                                
******  Copyright Fermi Research Alliance / Fermilab    
******            All Rights Reserved                             
*****
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-07CH11359 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws.
******                                                                
******                                                                
******  Author:    Chong Shik Park
******             Email: cspark@fnal.gov
******
******
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******
******  Aug 2014            cspark@fnal.gov
******  - implement non-linear lens first time
******  Aug 2016            chall
******  - expand around the cause where y is near 0 to avoid 0/0   
******
**************************************************************************
*************************************************************************/

#include <beamline/NonLinearLensPropagators.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/ParticleBunch.h>
#include <beamline/nonLinearLens.h>

using namespace std;
using FNAL::pcerr;
using FNAL::pcout;

namespace {

  Particle::PhaseSpaceIndex i_x   = Particle::xIndex;
  Particle::PhaseSpaceIndex i_y   = Particle::yIndex;
  Particle::PhaseSpaceIndex i_npx = Particle::npxIndex;
  Particle::PhaseSpaceIndex i_npy = Particle::npyIndex;

template<typename Particle_t>
void propagate( nonLinearLens& elm, Particle_t& p );

/* Derived from routines provided by Chad Mitchell with the following headers: */
/*
 !*****************************************************************
    ! The following subroutine computes the nonlinear momentum kick
    ! across a thin lens associated with a single short segment of the
    ! nonlinear magnetic insert described in V. Danilov and S. Nagaitsev,
    ! PRSTAB 13, 084002 (2010), Sect. V.A.  The arguments are as follows:
    !         knll - integrated strength of the lens (m)
    !         cnll - distance of singularities from the origin (m)
    !         coord = (x [m], px/p0, y [m], py/p0)
    ! This implementation is based on expressions in "Nonlinear Lens
    ! Tracking in the IOTA Complex Potential," C. Mitchell, Feb. 9, 2017.
    ! Variable definitions are chosen to be consistent with TRACK_EXT.
    ! C. Mitchell 2/9/2017
    !*****************************************************************
*/
/* The derivation is described in LBNL-1007217, Chad Mitchell,
 * __Complex Representation of Potentials and Fields for the Nonlinear
 * Magnetic Insert of the Integrable Optics Test Accelerator__,
 * Lawrence Berkeley National Lab, March 1, 2017.
 */

template<>
void propagate( nonLinearLens& elm, Particle& p )
{

  typedef PropagatorTraits<Particle>::State_t       State_t;
  typedef PropagatorTraits<Particle>::Component_t   Component_t;
  typedef PropagatorTraits<Particle>::ComplexComponent_t ComplexComponent_t;

  State_t& state = p.State();

  double const knll_ = elm.get_knll();
  double const cnll_ = elm.get_cnll();

  Component_t xbar = state[i_x] / cnll_;
  Component_t ybar = state[i_y] / cnll_;
  double kick = -knll_/cnll_;
    if ( ( ybar == 0 ) && ( abs(xbar) >= 1. ) ) {
//    (*pcerr) << "*** ERROR *** "
//             << "\n*** ERROR *** " << __FILE__ << ", " << __LINE__
//             << "\nNonLinearLensPropagator propagates with singular points: "
//             << xbar
//             << endl;
        state[i_npx] = std::numeric_limits<double>::quiet_NaN();
        state[i_npy] = std::numeric_limits<double>::quiet_NaN();
  } else {
        std::complex<double> c_i(0.0, 1.0);
        std::complex<double> c_1(1.0, 0.0);
        ComplexComponent_t zeta(xbar, ybar);
        ComplexComponent_t croot = sqrt(c_1 - zeta*zeta);
        ComplexComponent_t carcsin = -c_i * log(c_i*zeta + croot);
        ComplexComponent_t dF = zeta/(croot*croot) + carcsin/(croot*croot*croot);
        Component_t dPx = kick*real(dF);
        Component_t dPy = -kick*imag(dF);
        state[i_npx] += dPx;
        state[i_npy] += dPy;
    }
}

// JetParticles have some small complications that prevent fully templatization
template<>
void propagate( nonLinearLens& elm, JetParticle& p )
{

  typedef PropagatorTraits<JetParticle>::State_t       State_t;
  typedef PropagatorTraits<JetParticle>::Component_t   Component_t;
  typedef PropagatorTraits<JetParticle>::ComplexComponent_t ComplexComponent_t;

  State_t& state = p.State();

  double const knll_ = elm.get_knll();
  double const cnll_ = elm.get_cnll();

  Component_t xbar = state[i_x] / cnll_;
  Component_t ybar = state[i_y] / cnll_;
  double kick = -knll_/cnll_;
    if ( ( ybar.standardPart() == 0 ) && ( abs(xbar.standardPart()) >= 1. ) ) {
    (*pcerr) << "*** ERROR *** "
             << "\n*** ERROR *** " << __FILE__ << ", " << __LINE__
             << "\nNonLinearLensPropagator propagates with singular points: "
             << xbar
             << endl;
    throw std::runtime_error("can't propagate in singular region");
  }
    std::complex<double> c_i(0.0, 1.0);
    std::complex<double> c_1(1.0, 0.0);
    ComplexComponent_t zeta = xbar + c_i*ybar; // no constructor for complex component_t from real components
    ComplexComponent_t croot = sqrt(c_1 - zeta*zeta);
    ComplexComponent_t carcsin = -c_i * log(c_i*zeta + croot);
    ComplexComponent_t dF = zeta/(croot*croot) + carcsin/(croot*croot*croot);
    Component_t dPx = kick*real(dF);
    Component_t dPy = -kick*imag(dF);
    state[i_npx] += dPx;
    state[i_npy] += dPy;
}


//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------

#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void propagate(     nonLinearLens& elm,    Particle& p );
template void propagate(     nonLinearLens& elm, JetParticle& p );

#endif

} // namespace

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void nonLinearLens::Propagator::setup( nonLinearLens& elm )
{}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void nonLinearLens::Propagator::operator()( nonLinearLens& elm, Particle& p ) 
{
  ::propagate(elm ,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void nonLinearLens::Propagator::operator()( nonLinearLens& elm, JetParticle& p ) 
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

namespace {
} // anonymous namespace
