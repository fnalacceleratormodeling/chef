/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      InducedSextupoleKick.cc
******                                                                
******  Copyright (c) Fermi Research Alliance, LLC
******                Universities Research Association, Inc.
******                Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contracts No. DE-AC02-76CH03000
******  and No. DE-AC02-07CH11359.
******
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Author:    Eric Stern    egstern@fnal.gov
******             Qiming Lu     qlu@fnal.gov
******                                                                
******                                                                
******  ----------------
******  REVISION HISTORY
******  ----------------
******
******  Sep 2017           qlu@fnal.gov
******  - initial version.
******  - implementation of class InducedSextupoleKick, a magnetic kick
******    to be used in CF_sbend magnets.
******  - uses expressions for induced, effective magnetic fields 
******    for cylindrical geometry, derived independently
******    by Timofey Zolkin (2015) and Edwin McMillan (1975).
******  - implemented based on the InducedKick class
******  
**************************************************************************
*************************************************************************/




#include <iomanip>
#include <beamline/InducedSextupoleKick.h>
#include <beamline/InducedSextupoleKickPropagators.h>
#include <beamline/BmlVisitor.h>
#include <beamline/Alignment.h>

using namespace std;

// **************************************************
//   class InducedSextupoleKick
// **************************************************

InducedSextupoleKick::InducedSextupoleKick() : bmlnElmnt()
{
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

InducedSextupoleKick::InducedSextupoleKick( const char* n )
 :bmlnElmnt(n, 0.0, 0.0), R0(0.0)
{
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// Radius of curvature R0, integrated sextupole strength k
// k becomes chef strength, R0
//
// The sextupole component magnetic field of a combined function sbend is
//
// B_x = k R_0 {y \frac { (R_0 + x ) }
// B_y = k R_0 \log \left ( 1 + {x \frac R_0} \right )
InducedSextupoleKick::InducedSextupoleKick( const char* n, double k, double R0 )
: bmlnElmnt( n, 0.0, k), R0(R0)
{
  propagator_ = PropagatorPtr( new Propagator() );
  propagator_->setup(*this);
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
InducedSextupoleKick::InducedSextupoleKick( InducedSextupoleKick const& oldfsk) :
    bmlnElmnt(oldfsk), R0(oldfsk.get_R0()), propagator_(oldfsk.propagator_->Clone())
{
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

InducedSextupoleKick&  InducedSextupoleKick::operator=( InducedSextupoleKick const& rhs) 
{
  if ( &rhs == this ) return *this;  
  bmlnElmnt::operator=(rhs);
  propagator_ = PropagatorPtr(rhs.propagator_->Clone());

  return *this; 
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

InducedSextupoleKick::~InducedSextupoleKick() 
{ }


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

const char* InducedSextupoleKick::Type() const 
{ 
  return "InducedSextupoleKick"; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

bool InducedSextupoleKick::isMagnet() const 
{ 
  return true; 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::Split( double pc, ElmPtr& a, ElmPtr& b ) const
{
  if( ( pc <= 0.0 ) || ( pc >= 1.0 ) ) {
    ostringstream uic;
    uic  << "pc = " << pc << ": this should be within [0,1].";
    throw( GenericException( __FILE__, __LINE__, 
           "void InducedSextupoleKick::Split( double pc, bmlnElmnt** a, bmlnElmnt** b )", 
           uic.str().c_str() ) );
  }

  a = InducedSextupoleKickPtr( Clone() );
  b = InducedSextupoleKickPtr( Clone() );

  a->setLength( pc        * length_ );
  b->setLength( (1.0- pc) * length_ );


  // We assume "strength" field*length_.  This is not normal,
  // but the kicks are non-physical, fictitious elements.
  // It is because of this that these lines are needed
  // and using the default bmlnElmnt::Split method
  // produces a wrong result.
  // ------------------------
  a->setStrength( pc        * strength_ );
  b->setStrength( (1.0- pc) * strength_ );


  // Set the alignment struct
  // : this is a STOPGAP MEASURE!!!
  // ------------------------------
  a->setAlignment( Alignment() );
  b->setAlignment( Alignment() );

  // Rename
  a->rename( ident_ + string("_1") );
  b->rename( ident_ + string("_2") );
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::accept( BmlVisitor& v ) 
{ 
  v.visit( *this ); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::accept( ConstBmlVisitor& v ) const 
{
  v.visit( *this ); 
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void InducedSextupoleKick::usePropagator( PropagatorPtr& x )
{
  propagator_ = PropagatorPtr( x->Clone() );
  propagator_->setup( *this );
}


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void InducedSextupoleKick::localPropagate( Particle& p) 
{ 
  (*propagator_)(*this,p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::localPropagate( JetParticle& p) 
{ 
  (*propagator_)(*this,p);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::localPropagate( ParticleBunch& b) 
{ 
  (*propagator_)(*this,b);
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void InducedSextupoleKick::localPropagate( JetParticleBunch& b) 
{ 
  (*propagator_)(*this,b);
}

