/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                
******  BEAMLINE:  C++ objects for design and analysis
******             of beamlines, storage rings, and   
******             synchrotrons.                      
******                                    
******  File:      KickPropagators.cc
******                                                                
******  Copyright Fermi Research Alliance / Fermilab    
******            All Rights Reserved                             
*****
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-07CH11359 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws.
******                                                                
******  Authors:    Jean-Francois Ostiguy ostiguy@fnal.gov
******              Leo Michelotti        michelotti@fnal.gov                             
******              
******                                                                
**************************************************************************
*************************************************************************/


#include <basic_toolkit/iosetup.h>
#include <basic_toolkit/PhysicsConstants.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/elens.h>
#include <beamline/ElensPropagators.h>

using namespace std;
using FNAL::pcerr;
using FNAL::pcout;

namespace {

  Particle::PhaseSpaceIndex i_x   = Particle::xIndex;
  Particle::PhaseSpaceIndex i_y   = Particle::yIndex;
  Particle::PhaseSpaceIndex i_cdt = Particle::cdtIndex;
  Particle::PhaseSpaceIndex i_npx = Particle::npxIndex;
  Particle::PhaseSpaceIndex i_npy = Particle::npyIndex;
  Particle::PhaseSpaceIndex i_ndp = Particle::ndpIndex;


template<typename Particle_t>
void driftpropagate( double length, bmlnElmnt& elm, Particle_t& p )
{
     typedef typename PropagatorTraits<Particle_t>::State_t       State_t;
     typedef typename PropagatorTraits<Particle_t>::Component_t   Component_t;
 
     State_t& state = p.State();

     Component_t npz = p.get_npz();

     Component_t xpr = state[i_npx] / npz;
     Component_t ypr = state[i_npy] / npz;
    

     state[i_x] += length* xpr;
     state[i_y] += length* ypr;

     state[i_cdt] += length*sqrt( 1.0 + xpr*xpr + ypr*ypr )/ p.Beta(); 
}

// The derivation of the formulas used in this method are in synergia2/docs/e_lens_kick/elens_kick.pdf
static const double small_radius = 1.0e-12;

    // F(r) = A (1 - e^\frac{r^2}{2 \sigma^2}) \frac{1}{r}
    // write this out in components
    //
    // F_x = A  (1 - e^\frac{r^2}{2 \sigma^2}) \frac{1}{r} \frac{x}{r}
    // similarly for F_y
    // F_x = A  (1 - e^\frac{r^2}{2 \sigma^2}) \frac{x}{r^2}
    // expand around r close to 0
    // F_x \sim A \frac{x^2}{2\sigma^2} \frac{x}{r^2}

void applyElensKick( elens& elm, Particle& p)
{
    typedef PropagatorTraits<Particle>::State_t       State_t;
    typedef PropagatorTraits<Particle>::Component_t   Component_t;

    // std::cout << "egs: applyElensKick(Particle): strength: " << elm.Strength() << std::endl;
    if (elm.Strength() == 0.0) {
        return;
    }
    const double current_over_e =  elm.Strength()/PH_MKS_e;
    const double elens_length = elm.Length();
    const double radius = elens::elens_core_access::get_radius(elm);
    const double longrms = elens::elens_core_access::get_longrms(elm);
    //std::cout << "egs: ElensPropagators: longrms: " << longrms << std::endl;
    const elens::e_profile_t prof = elens::elens_core_access::get_profile(elm);
    const double gamma_e = (elens::elens_core_access::get_eenergy(elm)+PH_NORM_me)/PH_NORM_me;
    const double beta_e = std::sqrt(1.0 - 1.0/(gamma_e*gamma_e));
    const double gamma_b = p.ReferenceGamma();
    const double beta_b = p.ReferenceBeta();

    // horribly inefficient that we have to do these calculations every particle every time
    // if length == 0.0, then current is integrated current*length
    double integrated_strength;
    if (elens_length == 0.0) {
        integrated_strength = current_over_e;
    } else {
        // nonzero length electron lens was disabled at the time the longitudinal profile
        // was added because I didn't want to calculate the interaction between them.
        throw std::runtime_error("nonzero length electron lens not implemented");
        integrated_strength = current_over_e * elens_length;
    }

    State_t& state = p.State();

    Component_t betagamma_p = (1.0 + state[i_ndp])*p.ReferenceBeta()*p.ReferenceGamma();
    Component_t beta_p = betagamma_p/sqrt(1.0 + betagamma_p*betagamma_p);

    Component_t factors = -2.0 * integrated_strength * PH_MKS_rp * (1.0 + beta_e*beta_p)/(beta_e*beta_p*beta_b*gamma_b*PH_MKS_c);

    Component_t x = state[i_x];
    Component_t y = state[i_y];
    Component_t rsq = x*x + y*y;
    Component_t z = state[i_cdt]*p.ReferenceBeta();
    //std::cout << "egs: ElensPropagators ReferenceBeta, cdt, z: " << p.ReferenceBeta() << ", "
    //          << p.get_cdt() << ", " << p.ReferenceBeta()*p.get_cdt() << std::endl;

    Component_t long_factor;
    if (longrms <= 0.0) {
        long_factor = 1.0;
    } else {
        long_factor = exp(-z*z/(2.0*longrms*longrms));
    }
    //std::cout << "egs: ElensPropagators z: " << std::setprecision(16) << z << " long_factor: " << long_factor << std::endl;

    factors *= long_factor;

    const double twosigmasq = 2.0 * radius * radius;
    
    switch(prof) {
    case elens::gaussian:
        // 1 - exp(-r^2/(2 sigma^2) expands to
        //
        // r^2/(2 sigma^2) + (1/2)*(r^4/(4 sigma^4)) + (1/6)*(r^6/(8 sigma^6))

        if (rsq/(radius*radius) < small_radius) {
            state[i_npx] += x * factors * (1.0/twosigmasq - 0.5 * rsq/(twosigmasq*twosigmasq) +
                                           (1.0/6.0) * rsq*rsq/(twosigmasq*twosigmasq*twosigmasq));
            state[i_npy] += y * factors * (1.0/twosigmasq - 0.5 * rsq/(twosigmasq*twosigmasq) +
                                           (1.0/6.0) * rsq*rsq/(twosigmasq*twosigmasq*twosigmasq));
        } else {
            state[i_npx] += factors*(1.0 - exp(-rsq/twosigmasq)) * (x/rsq);
            state[i_npy] += factors*(1.0 - exp(-rsq/twosigmasq)) * (y/rsq);
        }
        break;
    case elens::uniform:
        state[i_npx] += factors * x/(radius*radius);
        state[i_npy] += factors * y/(radius*radius);
        break;
    default:
        throw( GenericException( __FILE__, __LINE__,
               "applyElensKick",
               "electron profile is undefined" ) );
    }
}

//////////////////////////////////////////////////////////////////////////

void applyElensKick( elens& elm, JetParticle& p)
{
    typedef PropagatorTraits<JetParticle>::State_t       State_t;
    typedef PropagatorTraits<JetParticle>::Component_t   Component_t;

    // std::cout << "egs: applyElensKick(JetParticle): strength: " << elm.Strength() << std::endl;
    if (elm.Strength() == 0.0) {
        return;
    }
    const double current_over_e =  elm.Strength()/PH_MKS_e;
    const double elens_length = elm.Length();
    const double radius = elens::elens_core_access::get_radius(elm);
    const double longrms = elens::elens_core_access::get_longrms(elm);
    const elens::e_profile_t prof = elens::elens_core_access::get_profile(elm);
    const double gamma_e = (elens::elens_core_access::get_eenergy(elm)+PH_NORM_me)/PH_NORM_me;
    const double beta_e = std::sqrt(1.0 - 1.0/(gamma_e*gamma_e));
    const double gamma_b = p.ReferenceGamma();
    const double beta_b = p.ReferenceBeta();

    // horribly inefficient that we have to do these calculations every particle every time
    // if length == 0.0, then current is integrated current*length
    double integrated_strength;
    if (elens_length == 0.0) {
        integrated_strength = current_over_e;
    } else {
        throw std::runtime_error("nonzero length electron lens not implemented");
        integrated_strength = current_over_e * elens_length;
    }

    State_t& state = p.State();

    Component_t betagamma_p = (1.0 + state[i_ndp])*p.ReferenceBeta()*p.ReferenceGamma();
    Component_t beta_p = betagamma_p/sqrt(1.0 + betagamma_p*betagamma_p);

    Component_t factors = -2.0 * integrated_strength * PH_MKS_rp * (1.0 + beta_e*beta_p)/(beta_e*beta_p*beta_b*gamma_b*PH_MKS_c);

    Component_t x = state[i_x];
    Component_t y = state[i_y];
    Component_t rsq = x*x + y*y;
    Component_t z = state[i_cdt]*p.ReferenceBeta();

    Component_t long_factor;
    if (longrms <= 0.0) {
        long_factor = 1.0;
    } else {
        long_factor = exp(-z*z/(2.0*longrms*longrms));
    }

    //std::cout << "egs: z: " << std::setprecision(16) << z << " << long_factor: " << long_factor << std::endl;
    factors *= long_factor;
    double twosigmasq = 2.0 * radius * radius;

    switch(prof) {
    case elens::gaussian:
        // 1 - exp(-r^2/(2 sigma^2) expands to
        //
        // r^2/(2 sigma^2) + (1/2)*(r^4/(4 sigma^4)) + (1/6)*(r^6/(8 sigma^6))

        if ((rsq/(radius*radius)).standardPart() < small_radius) {
            state[i_npx] += x * factors * (1.0/twosigmasq - 0.5 * rsq/(twosigmasq*twosigmasq) +
                                           (1.0/6.0) * rsq*rsq/(twosigmasq*twosigmasq*twosigmasq));
            state[i_npy] += y * factors * (1.0/twosigmasq - 0.5 * rsq/(twosigmasq*twosigmasq) +
                                           (1.0/6.0) * rsq*rsq/(twosigmasq*twosigmasq*twosigmasq));
        } else {
            state[i_npx] += factors*(1.0 - exp(-rsq/twosigmasq)) * (x/rsq);
            state[i_npy] += factors*(1.0 - exp(-rsq/twosigmasq)) * (y/rsq);
        }
        break;
    case elens::uniform:
        state[i_npx] += factors * x/(radius*radius);
        state[i_npy] += factors * y/(radius*radius);
        break;
    default:
        throw( GenericException( __FILE__, __LINE__,
               "applyElensKick",
               "electron profile is undefined" ) );
    }

    return;

}

template<typename Particle_t>
void propagate( elens& elm, Particle_t& p )
{
  typedef typename PropagatorTraits<Particle_t>::State_t       State_t;

  State_t& state = p.State();

  if (elm.Length() > 0.0 ) {
      throw std::runtime_error("nonzero length electron lens not implemented");


    ::driftpropagate( elm.Length()/2, elm, p );   // Drift through first  half of the length
    applyElensKick(elm,p);
    ::driftpropagate( elm.Length()/2, elm, p );   // Drift through second half of the length.

    state[i_cdt] -= elm.getReferenceTime();  
 }
  else {
    applyElensKick(elm,p);
  }
 
 }

//----------------------------------------------------------------------------------
// Workaround for gcc < 4.2 mishandling of templates defined in anonymous namespace
//----------------------------------------------------------------------------------
#if (__GNUC__ == 3) ||  ((__GNUC__ == 4) && (__GNUC_MINOR__ < 2 ))

template void driftpropagate( double length, bmlnElmnt& elm, Particle& p );
template void driftpropagate( double length, bmlnElmnt& elm, JetParticle& p );

template void applyElensKick( elens& elm, Particle& p);
template void applyElensKick( elens& elm, JetParticle& p);

template void propagate( elens& elm,    Particle& p );
template void propagate( elens& elm, JetParticle& p );

#endif
//-----------------------------------------------------------------------------------


} // anonymous namespace

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::Propagator::setup( elens& elm )
{}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::Propagator::operator()( elens& elm, Particle& p )
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void elens::Propagator::operator()( elens& elm, JetParticle& p )
{
  ::propagate(elm,p);
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
