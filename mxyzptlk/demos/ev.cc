/*
**
** Demo program:
** 
** Jet evaluation: ev.cc
** 
** --- Leo Michelotti
** --- August 18, 1995
**
** --- Modified September 10, 2012
*/

#include <iostream>
#include <vector>

#include <mxyzptlk/mxyzptlk.h>

using namespace std;

main( int argc, char** argv ) 
{
  if( argc != 4 ) {
    cout << "\nUsage: " << argv[0] 
         << "  x y n\n"
         << endl;
    exit(-1);
  }
  cout << "Command line: " << argv[0];
  for( int i = 1; i < argc; ++i ) {
    cout << "  " << argv[i];
  }
  cout << "\n=================\n" << endl;

  int deg = atoi( argv[3] );
  
  Jet__environment::BeginEnvironment(deg);
  coord x( atof( argv[1] ) ), y( atof( argv[2] ) );
  Jet__environment::EndEnvironment();

  Jet f = exp(-x*x) * sin(y);

  std::vector<double> point(2);
  cout << "Enter x and y: ";
  cin  >> point[0] >> point[1];

  while( true ) {
    cout << "Jet answer: "
         << f( point ) 
         << "  Exact answer: " 
         << sin( point[1] ) * exp( - point[0]*point[0] )
         << endl;
    cout << "Enter x and y: ";
    cin  >> point[0] >> point[1];
  }
}
