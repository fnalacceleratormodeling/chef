/*************************************************************************
**************************************************************************
**************************************************************************
******
******  Mxyzptlk:  A C++ implementation of differential algebra.
******
******  File:      TJLterm.h
******
******  Copyright (c) Universities Research Association, Inc./ Fermilab
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000.
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
******  Author:    Leo Michelotti
******
******             Fermilab
******             Batavia, IL   60510
******             Email: michelotti@fnal.gov
******
******  Revision History:
******
******  Feb 2005 - Jean-Francois Ostiguy
*****              ostiguy@fnal.gov
******
******  - Efficiency improvements.
******  - new memory management scheme
******
******  Sep-Dec 2005  ostiguy@fnal.gov
******
****** - refactored code to use a single class template parameter
******   instead of two. Mixed mode operations now handled using
******   implicit conversion operators.
****** - reference counting now based on using boost::intrusive pointer
****** - reference counted TJetEnvironment
****** - centralized TJetEnvironment management
****** - all implementation details now completely moved to TJL
****** - redesigned coordinate class Tcoord. New class Tparams for parameters
****** - header files support for both explicit and implicit template instantiations
******   (default for mxyzptlk = explicit)
******   for explicit instantiations, define MXYZPTLK_EXPLICIT_TEMPLATES
******
******  Sep 2006
******
****** - eliminated archaic "Reconstruct" members. Use placement new syntax instead.
******
****** Mar 2007
****** - Introduced new compact monomial indexing scheme based on monomial ordering
******   to replace previous scheme based explicitly on monomial exponents tuples.
****** - monomial multiplication handled via a lookup-table.
******
**************************************************************************
*************************************************************************/
#ifndef TJLTERM_H
#define TJLTERM_H

#include <complex>
#include <ostream>
#include <boost/functional/hash/hash.hpp>
#include <boost/pool/pool.hpp>
#include <basic_toolkit/IntArray.h>
#include <mxyzptlk/EnvPtr.h>


template<typename T>
class TJetEnvironment;

template<typename T>
class ScratchArea;

template<typename T>
class TJLterm;

template<typename T>
bool operator<=(TJLterm<T> const&, TJLterm<T> const&);

template<typename T>
bool operator%=(TJLterm<T> const&, TJLterm<T> const&);

template<typename T>
bool operator==(TJLterm<T> const&, TJLterm<T> const&);

template<typename T>
std::ostream& operator<<(std::ostream& os, TJLterm<T> const&);


// ********************************************************************************************************************


template <typename T>
class DLLEXPORT TJLterm
{
 public:
  int      offset_ = 0;     //  The offset of this term in the scratchpad
  T        value_ = T();      //  The value associated with the JLterm.
  int      weight_ = 0;     //  The sum of the values in index.  For the above example,
                        //  this would be 4.
  // Constructors and destructors

  TJLterm() = default;
  TJLterm( IntArray  const&, const T&,  EnvPtr<T> const& pje );

  TJLterm( T const&, int offset, int weight );

  template<typename U>
  TJLterm( TJLterm<U> const& );

  bool    operator<( TJLterm<T> const& rhs) const;

  T         coefficient()                    const { return value_; }
  IntArray  exponents( EnvPtr<T> const& env) const;

 // JLterm array allocation functions

  static TJLterm<T>*       array_allocate(int n);
  static void              array_deallocate(TJLterm<T>* p);

  // ~TJLterm();           // MUST **NOT** BE DEFINED !

  private:

  static boost::pool<>&  ordered_memPool_;  // an ordered pool of TJLterms

};


//-----------------------------------------------------------------------------------
// specializations
//-----------------------------------------------------------------------------------

template<>
template<>
TJLterm<std::complex<double>>::TJLterm(TJLterm<double> const& );

#ifndef MXYZPTLK_EXPLICIT_TEMPLATES
#include <mxyzptlk/TJLterm.tcc>
#endif

#endif // TJLTERM_H
