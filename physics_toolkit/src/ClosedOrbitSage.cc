/*************************************************************************
**************************************************************************
**************************************************************************
******
******  PHYSICS TOOLKIT: Library of utilites and Sage classes
******             which facilitate calculations with the
******             BEAMLINE class library.
******
******  File:      ClosedOrbitSage.cc
******
******  Copyright (c) 2001  Universities Research Association, Inc.
******                All Rights Reserved
******
******  Author:    Leo Michelotti
******
******             Fermilab
******             P.O.Box 500
******             Mail Stop 220
******             Batavia, IL   60510
******
******             Phone: (630) 840 4956
******             Email: michelotti@fnal.gov
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
******
******  REVISION HISTORY
******
******  Mar 2007        ostiguy@fnal.gov
******  -reference counted elements
******  -use new-style beamline iterators
******  -eliminated references to slist/dlist
******
******  Jan 2008        ostiguy@fnal.gov
****** - modified to compute closed orbit using non-zero dp/p state component
******
**************************************************************************
*************************************************************************/



/*
 *  File: ClosedOrbitSage.cc
 *  The "Sage" for using FPSolver.
 *
 *  November 23, 1998
 *  Leo Michelotti
 *
 *  Modified:
 *  April 29, 2004
 *  Leo Michelotti
 */

#include <physics_toolkit/ClosedOrbitSage.h>
#include <physics_toolkit/FPSolver.h>
#include <beamline/RefRegVisitor.h>
#include <mxyzptlk/Jet__environment.h>
#include <beamline/rfcavity.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <list>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>


#define DEBUG 0

using namespace std;

namespace {
  Particle::PhaseSpaceIndex const& i_x   = Particle::xIndex;
  Particle::PhaseSpaceIndex const& i_y   = Particle::yIndex;
  Particle::PhaseSpaceIndex const& i_px  = Particle::npxIndex;
  Particle::PhaseSpaceIndex const& i_py  = Particle::npyIndex;

  int const BMLN_dynDim = 6;

 }

  int ClosedOrbitSage::ERR_NOTRING = -17;

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

ClosedOrbitSage::ClosedOrbitSage( BmlPtr x )
: Sage( x ), forced_(false), ignoreErrors_(false)
{ }


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

ClosedOrbitSage::ClosedOrbitSage( ClosedOrbitSage const & cos  )
: Sage( cos.myBeamlinePtr_ ), forced_(cos.forced_), ignoreErrors_(cos.ignoreErrors_)
{ }


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


void ClosedOrbitSage::setForcedCalc()
{
  forced_ = true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void ClosedOrbitSage::unsetForcedCalc()
{
  forced_ = false;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void ClosedOrbitSage::setIgnoreErrors( bool x )
{
  ignoreErrors_ = x;
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

struct Co_params {
    Particle prt;
    BmlPtr bmlptr;
    Co_params(Particle prt, BmlPtr b, double tolerance): prt(prt), bmlptr(b->Clone())
    {
        // turn off the RF cavities
        for ( beamline::deep_iterator it  = bmlptr->deep_begin(); 
            it != bmlptr->deep_end(); ++it )
        {
            if( ( dynamic_cast<rfcavity*>( (*it).get() ) ) ||
                ( dynamic_cast<thinrfcavity*>( (*it).get() ) ) ) {
                (*it)->setStrength( 0.0 );
            }
        }
    }
};

int
propagate_co_try(const gsl_vector *co_try, void *params, gsl_vector *co_results)
{
    Co_params *copp = static_cast<Co_params *>(params);
    
    double x0 = gsl_vector_get(co_try, 0);
    double x1 = gsl_vector_get(co_try, 1);
    double x2 = gsl_vector_get(co_try, 2);
    double x3 = gsl_vector_get(co_try, 3);
    
    Particle p(copp->prt);
    p.set_x(x0);
    p.set_npx(x1);
    p.set_y(x2);
    p.set_npy(x3);
    p.set_cdt(0.0);
    p.set_ndp(copp->prt.get_ndp());
    copp->bmlptr->propagate(p);
    gsl_vector_set(co_results, 0, p.get_x()-x0);
    gsl_vector_set(co_results, 1, p.get_npx()-x1);
    gsl_vector_set(co_results, 2, p.get_y()-x2);
    gsl_vector_set(co_results, 3, p.get_npy()-x3);
    return GSL_SUCCESS;
}

// find closed orbit using gsl fit of particle (no jet environment necessary)
int ClosedOrbitSage::findClosedOrbit(Particle& p, double tolerance)
{
    Co_params co_params(p, this->myBeamlinePtr_, tolerance);
    const size_t ndim = 4; // solve closed orbit in x, xp, y, yp
    const gsl_multiroot_fsolver_type * T = gsl_multiroot_fsolver_hybrids;
    gsl_multiroot_fsolver * solver = gsl_multiroot_fsolver_alloc(T, ndim);

    // co_try are the coordinates of the closed orbit
    gsl_vector *co_try = gsl_vector_alloc(ndim);
    // initialize the closed orbit
    gsl_vector_set(co_try, 0, p.get_x());
    gsl_vector_set(co_try, 1, p.get_npx());
    gsl_vector_set(co_try, 2, p.get_y());
    gsl_vector_set(co_try, 3, p.get_npy());
    gsl_multiroot_function F;
    F.f = &propagate_co_try;
    F.n = ndim;
    F.params = &co_params;

    gsl_multiroot_fsolver_set(solver, &F, co_try);
#if DEBUG
    std::cout << "after gsl_multiroot_fsolver_set" << std::endl;
#endif
    int niter=0;
    const int maxiter = 100;
    do {
        int rc;
#if DEBUG
        std::cout << "egs: starting iter " << niter << std::endl;
#endif
        rc = gsl_multiroot_fsolver_iterate(solver);
        switch(rc) {
        case GSL_ENOPROG:
#if DEBUG
            std::cout << "egs: ENOPROG" << std::endl;
#endif
            throw std::runtime_error("Closed orbit solver unable to converge.  Is the tolerance too tight?");
            break;
        case GSL_EBADFUNC:
#if DEBUG
            std::cout << "egs: EBADFUNC" << std::endl;
#endif
            throw std::runtime_error("Closed orbit solver failed to evaluate solution");
            break;
        default:
#if DEBUG
            std::cout << "egs: success" << std::endl;
#endif
            break;
        }
#if DEBUG
        std::cout << "egs: residuals at current iteration: " << gsl_vector_get(solver->f, 0) << ", "
            << gsl_vector_get(solver->f, 1) << ", " << gsl_vector_get(solver->f, 2) <<
            ", " << gsl_vector_get(solver->f, 3) << std::endl;
        gsl_vector *froots = gsl_multiroot_fsolver_root(solver);
        std::cout << "egs: roots at current iteration: " << gsl_vector_get(solver->x, 0) << ", "
            << gsl_vector_get(solver->x, 1) << ", " << gsl_vector_get(solver->x, 2) <<
            ", " << gsl_vector_get(solver->x, 3) << std::endl;
#endif
    } while ((gsl_multiroot_test_residual(solver->f, tolerance) == GSL_CONTINUE) && (++niter < maxiter));
    if (niter == maxiter) {
        std::stringstream sstr;
        sstr << "Could not locate closed orbit after " << maxiter << " iterations";
        throw std::runtime_error( sstr.str() );
    }
    gsl_vector *froots = gsl_multiroot_fsolver_root(solver);
    p.set_x(gsl_vector_get(froots, 0));
    p.set_npx(gsl_vector_get(froots, 1));
    p.set_y(gsl_vector_get(froots, 2));
    p.set_npy(gsl_vector_get(froots, 3));

    gsl_multiroot_fsolver_free(solver);
    gsl_vector_free(co_try);
    return 0;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

int ClosedOrbitSage::findClosedOrbit( JetParticle& jp )
{
    // for now, use the solver algorithm with jets
    return findClosedOrbit_withJets( jp );

    // this one is not currently used.  It break Synergia
    // attempt to use the particle algorithm to find the closed orbit
    // turn it into a jet and do the jet magic that is expected.

    // first find the closed orbit
    Particle p(jp);
    int rc = findClosedOrbit(p);
    if (rc) {
        return rc;
    }

    // now do jet stuff
    Jet__environment_ptr  storedEnv  = Jet__environment::getLastEnv();
    JetC__environment_ptr storedEnvC = JetC__environment::getLastEnv();

    Jet__environment::setLastEnv( jp.State().Env() );

    // Turn off all RF ...

    std::list<strengthData> cavityStrengths;

    for ( beamline::deep_iterator it  = myBeamlinePtr_->deep_begin(); 
          it != myBeamlinePtr_->deep_end(); ++it )
    {
        if( ( dynamic_cast<rfcavity*>( (*it).get() ) )
            || ( dynamic_cast<thinrfcavity*>( (*it).get() ) ) )
        {
            strengthData sd;
            sd.address = (*it);
            sd.strength = (*it)->Strength();
            cavityStrengths.push_back(sd);
            (*it)->setStrength( 0.0 );
        }
    }

    jp = JetParticle(p);
    invokeFPSolver(jp);

    // Register the closed orbit with the beamline.
    // NOTE: this alters the line.

    if( verbose_ ) {
        *outputStreamPtr_ << "ClosedOrbitSage --- Registering closed orbit: will change line."
                          << endl;
    }

    Particle prt(p);
    RefRegVisitor registrar( prt );  
    myBeamlinePtr_->accept( registrar );

    prt = Particle(p);
    prt.set_cdt(0.0);

    Jet__environment::setLastEnv( Jet__environment::getApproxJetEnvironment(Jet__environment::getLastEnv()->maxWeight(), prt.State() ));
  
    bool newcoords = false;
    coord*  tmpcoord[BMLN_dynDim];

    if ( !Jet__environment::getLastEnv() ) {  // true if there was no suitable approximate Environment  
 
        Jet__environment::BeginEnvironment( jp.State().Env()->maxWeight() );
        for( int i = 0; i < BMLN_dynDim; ++i) { 
            tmpcoord[i] = new coord(prt.State()[i]); 
        }
        Jet__environment::EndEnvironment();
        //    Jet::_lastEnv  = Jet__environment::EndEnvironment();
        //    JetC::_lastEnv = Jet::_lastEnv;

        newcoords = true;
    }

    // Instantiate a JetParticle with the new environment
    // and propagate it once around the ring.

    if( verbose_ ) {
        *outputStreamPtr_ << "ClosedOrbitSage --- Propagating JetParticle around ring final time."
                          << endl;
    }

    JetParticle jpr2Ptr(prt, Jet__environment::getLastEnv() );
    myBeamlinePtr_->propagate( jpr2Ptr );

    // Reset the argument to this second JetParticle

    jp = jpr2Ptr;

    // Final operations ....................................

    // ... Return RF to original settings before leaving ...

    for ( std::list<strengthData>::iterator it  = cavityStrengths.begin();
          it != cavityStrengths.end(); ++it )
    {
        ( it->address )->setStrength( it->strength );
    }


    // ... Reset the Jet environment to its incoming state

    Jet__environment::setLastEnv(storedEnv);
    JetC__environment::setLastEnv(storedEnvC);

    // ... delete temporary coordinates 
    if( newcoords ) {
        for( int i=0; i<BMLN_dynDim; i++ ) { 
            delete tmpcoord[i]; 
        }
    }

    if( verbose_ ) {
        *outputStreamPtr_ << "ClosedOrbitSage -- Leaving ClosedOrbitSage::findClosedOrbit"
                          << endl;
    }
    return 0;
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


int ClosedOrbitSage::findClosedOrbit_withJets( JetParticle& jp )
{
  if( verbose_ ) {
    *outputStreamPtr_ << "ClosedOrbitSage -- Entering ClosedOrbitSage::findClosedOrbit"
                      << endl;
  }

  // If the line is not to be treated as a ring, abort the calculation.
  if( !isTreatedAsRing() ) {
    if( !ignoreErrors_ ) { return ERR_NOTRING; }
  }

  // Preliminary steps ...

  Jet__environment_ptr  storedEnv  = Jet__environment::getLastEnv();
  JetC__environment_ptr storedEnvC = JetC__environment::getLastEnv();

  Jet__environment::setLastEnv( jp.State().Env() );


  // Helper particle ...

  Particle prt(jp);

  Vector inState( prt.State() );

  // Turn off all RF ...

  std::list<strengthData> cavityStrengths;

  for ( beamline::deep_iterator it  = myBeamlinePtr_->deep_begin(); 
                                it != myBeamlinePtr_->deep_end(); ++it )
  {
    if(         ( dynamic_cast<rfcavity*>( (*it).get() ) )
	     || ( dynamic_cast<thinrfcavity*>( (*it).get() ) ) )
    {
      strengthData sd;
      sd.address = (*it);
      sd.strength = (*it)->Strength();
      cavityStrengths.push_back(sd);
      (*it)->setStrength( 0.0 );
    }
  }


  int i, ret;
  

  // If the calculation is forced, calculate the closed orbit.
  if( forced_ )
  {
    if( 0 != ( ret = invokeFPSolver( jp ) ) ) {
      if( !ignoreErrors_ ) { return ret; }
    }
  }

  // If not, check to see if the Particle is on a transverse closed orbit.
  else
  { 
    myBeamlinePtr_->propagate( prt );
    // *** CHANGE ***
    // *** CHANGE *** See above re tolerances
    // *** CHANGE ***
    if( ( std::abs( inState(i_x)  - prt.State()[i_x]  ) > 1.0e-6 ) ||
        ( std::abs( inState(i_y)  - prt.State()[i_y]  ) > 1.0e-6 ) ||
        ( std::abs( inState(i_px) - prt.State()[i_px] ) > 1.0e-6 ) ||
        ( std::abs( inState(i_py) - prt.State()[i_py] ) > 1.0e-6 )
    ){
      if( 0 != ( ret = invokeFPSolver( jp ) ) ) {
        if( !ignoreErrors_ ) { return ret; }
      }
    }
  }


  if( verbose_ ) {
    *outputStreamPtr_ << "ClosedOrbitSage --- Closed orbit successfully calculated."
         << endl;
  }

  // Register the closed orbit with the beamline.
  // NOTE: this alters the line.

  if( verbose_ ) {
    *outputStreamPtr_ << "ClosedOrbitSage --- Registering closed orbit: will change line."
         << endl;
  }

 
  prt = Particle(jp);
  RefRegVisitor registrar( prt );  
  myBeamlinePtr_->accept( registrar );

  // Useful Particle and Vector
  // containing the closed orbit.

  prt = Particle(jp);
  prt.set_cdt(0.0);

  Jet__environment::setLastEnv( Jet__environment::getApproxJetEnvironment(Jet__environment::getLastEnv()->maxWeight(), prt.State() ));
  
 

  bool newcoords = false;
  coord*  tmpcoord[BMLN_dynDim];

  if ( !Jet__environment::getLastEnv() ) {  // true if there was no suitable approximate Environment  
 
    Jet__environment::BeginEnvironment( jp.State().Env()->maxWeight() );
    for( i = 0; i < BMLN_dynDim; ++i) { 
      tmpcoord[i] = new coord(prt.State()[i]); 
    }
    Jet__environment::EndEnvironment();
    //    Jet::_lastEnv  = Jet__environment::EndEnvironment();
    //    JetC::_lastEnv = Jet::_lastEnv;

    newcoords = true;
  }

  // Instantiate a JetParticle with the new environment
  // and propagate it once around the ring.

  if( verbose_ ) {
    *outputStreamPtr_ << "ClosedOrbitSage --- Propagating JetParticle around ring final time."
         << endl;
  }

  JetParticle jpr2Ptr(prt, Jet__environment::getLastEnv() );
  myBeamlinePtr_->propagate( jpr2Ptr );
  

  // Reset the argument to this second JetParticle

  jp = jpr2Ptr;

  // Final operations ....................................

  // ... Return RF to original settings before leaving ...

  for ( std::list<strengthData>::iterator it  = cavityStrengths.begin();
	it != cavityStrengths.end(); ++it )
  {
    ( it->address )->setStrength( it->strength );
  }


  // ... Reset the Jet environment to its incoming state

  Jet__environment::setLastEnv(storedEnv);
  JetC__environment::setLastEnv(storedEnvC);

  // ... delete temporary coordinates 
  if( newcoords ) {
    for( i=0; i<BMLN_dynDim; i++ ) { 
      delete tmpcoord[i]; 
    }
  }

  if( verbose_ ) {
    *outputStreamPtr_ << "ClosedOrbitSage -- Leaving ClosedOrbitSage::findClosedOrbit"
         << endl;
  }

  

  return 0;
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

int ClosedOrbitSage::invokeFPSolver( JetParticle& jp )
{
  if( verbose_ ) {
    *outputStreamPtr_
      << "ClosedOrbitSage --- Starting calculation of closed orbit."
      << endl;
  }
  
  FPSolver fp( myBeamlinePtr_ );

#if 0

  double jumpScale [] = { 1.0e-6, 1.0e-6, 1.0e-6, 1.0e-6, 1.0e-6, 1.0e-6 };
  double zeroScale [] = { 1.0e-9, 1.0e-9, 1.0e-9, 1.0e-9, 1.0e-9, 1.0e-9 };

  for( int i = 0; i < BMLN_dynDim; i++ ) fp.ZeroScale(i) = zeroScale[i];
  for( int i = 0; i < BMLN_dynDim; i++ ) fp.JumpScale(i) = jumpScale[i];

#endif

  int fpError = fp( jp, "transverse", Sage::no );

  if(fpError)
  {
    *errorStreamPtr_
         << "\n*** ERROR ***"
            "\n*** ERROR *** File: " << __FILE__ << " Line: " << __LINE__
         << "\n*** ERROR *** ClosedOrbitSage::invokeFPSolver"
            "\n*** ERROR *** The FPSolver failed to find a solution."
            "\n*** ERROR *** Return error code = " << fpError
         << "\n*** ERROR *** "
         << endl;
    return 2;
  }


  // Test the new closed orbit ...

  Particle prt(jp);
  Vector co = prt.State();

  myBeamlinePtr_->propagate( prt );

  // *** CHANGE ***
  // *** CHANGE *** Why one micron?
  // *** CHANGE *** Shouldn't the stopping criterion be the same
  // *** CHANGE *** as was used by the FPSolver fp?

  if( ( std::abs( co(i_x)  - prt.State()[i_x]  ) > 2.0e-6 ) ||
      ( std::abs( co(i_y)  - prt.State()[i_y]  ) > 2.0e-6 ) ||
      ( std::abs( co(i_px) - prt.State()[i_px] ) > 2.0e-6 ) ||
      ( std::abs( co(i_py) - prt.State()[i_py] ) > 2.0e-6 )
  ){
    *errorStreamPtr_
         << "\n*** WARNING ***"
            "\n*** WARNING *** File: " << __FILE__ << " Line: " << __LINE__
         << "\n*** WARNING *** ClosedOrbitSage::invokeFPSolver"
            "\n*** WARNING *** Closed orbit not correct."
            "\n*** WARNING *** delta x = "
         << (std::abs(co(i_x)  - prt.State()[i_x]))
         << "\n*** WARNING *** delta y = "
         << (std::abs(co(i_y)  - prt.State()[i_y]))
         << "\n*** WARNING *** delta p_x/p = "
         << (std::abs(co(i_px)  - prt.State()[i_px]))
         << "\n*** WARNING *** delta p_y/p = "
         << (std::abs(co(i_py)  - prt.State()[i_py]))
         << "\n*** WARNING ***"
         << endl;

    if( verbose_ ) {
      *outputStreamPtr_ << "ClosedOrbitSage -- Leaving  ClosedOrbitSage::invokeFPSolver with error"
                        << endl;
    }

    return 1;  
  }

  return 0;
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void ClosedOrbitSage::eraseAll()
{
  *outputStreamPtr_ << "ClosedOrbitSage::eraseAll() does nothing." << endl;
}
