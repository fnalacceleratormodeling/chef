/*************************************************************************
**************************************************************************
**************************************************************************
******
******  PHYSICS TOOLKIT: Library of utilites and Sage classes
******             which facilitate calculations with the
******             BEAMLINE class library.
******
******  File:      normalForm.cc
******
******  Copyright  Universities Research Association, Inc / Fermilab.
******             All Rights Reserved
******
******  Author:    Leo Michelotti
******
******             Fermilab
******             P.O.Box 500
******             Batavia, IL   60510
******
******             Phone: (630) 840 4956
******             Email: michelotti@fnal.gov
******
******  Usage, modification, and redistribution are subject to terms
******  of the License supplied with this software.
******
******  Software and documentation created under
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000.
******  The U.S. Government retains a world-wide non-exclusive,
******  royalty-free license to publish or reproduce documentation
******  and software for U.S. Government purposes. This software
******  is protected under the U.S. and Foreign Copyright Laws.
******
****** REVISION HISTORY
****** ----------------
****** Mar 2007       ostiguy@fnal.gov
****** - some efficiency improvements
****** - use new style Jet iterators.
******
****** Aug 2008       ostiguy@fnal
****** - templated version of "shear" function
******
****** Jan 2011       michelotti@fnal.gov
****** - Eric Stern extended the algorithm
******   from four to six dimensions by adopting
******   the normalization and ordering procedures
******   to the "longitudinal" sector.
******
******
******  Dec 2015       michelotti@fnal.gov
******  - added a warning when tunes are too close to a
******    resonance.  Behavior has not been altered. The
******    warning is informative only, alerting the user
******    that a resonance term has not been subtracted.
******    This reduces the effectiveness of the
******    transformation, but it always has been so.
******
******  Mar 2017       michelotti@fnal.gov
******  - added an exception to be thrown early if a linear
******    tune is too close to an integer, half-integer or 
******    quarter-integer value.
******  - added a test so that shear terms would not
******    trigger warnings about resonance subtraction.
******  - added resonance identification to the warning,
******    in the case of legitimate resonances.
******  - changed streaming for the "something's wacko" warning.
******
**************************************************************************
*************************************************************************/

#include <boost/shared_ptr.hpp>
#include <vector>

/*
**
** Utility function.
**
** Calculates the nonresonant normal form
** of a one-turn map.
**
** --- Leo Michelotti
** --- First version: November 4, 1993
** --- Final version: March 3, 1994
**
*/

#include <basic_toolkit/iosetup.h>
#include <basic_toolkit/GenericException.h>
#include <basic_toolkit/Matrix.h>
#include <mxyzptlk/mxyzptlk.h>

using FNAL::pcout;
using FNAL::pcerr;

using namespace std;   // possibly not necessary in this file

#define MLT1  1.0e-6

bool sh0( const IntArray& index, const std::complex<double>& /* value */ ) {
 if( index(0) != index(3) + 1 ) return false;
 if( index(1) != index(4) )     return false;
 if( index(2) != index(5) )     return false;
 return true;
}

bool sh1( const IntArray& index, const std::complex<double>& /* value */ ) {
 if( index(0) != index(3) )     return false;
 if( index(1) != index(4) + 1 ) return false;
 if( index(2) != index(5) )     return false;
 return true;
}

bool sh2( const IntArray& index, const std::complex<double>& /* value */ ) {
 if( index(0) != index(3) )     return false;
 if( index(1) != index(4) )     return false;
 if( index(2) != index(5) + 1 ) return false;
 return true;
}

bool sh3( const IntArray& index, const std::complex<double>& /* value */ ) {
 if( index(0) != index(3) - 1 ) return false;
 if( index(1) != index(4) )     return false;
 if( index(2) != index(5) )     return false;
 return true;
}

bool sh4( const IntArray& index, const std::complex<double>& /* value */ ) {
 if( index(0) != index(3) )     return false;
 if( index(1) != index(4) - 1 ) return false;
 if( index(2) != index(5) )     return false;
 return true;
}

bool sh5( const IntArray& index, const std::complex<double>& /* value */ ) {
 if( index(0) != index(3) )     return false;
 if( index(1) != index(4) )     return false;
 if( index(2) != index(5) - 1 ) return false;
 return true;
}

// ===============================================================

void normalForm( const Mapping& theMapping, /* input */
                 int            maxOrder,   /* input */
                 MatrixC*       Bptr,
                 CLieOperator*  N,
                 CLieOperator*  T ) 
{
 const std::complex<double> complex_0 (0.0, 0.0);
 const std::complex<double> complex_1 (1.0, 0.0);
 const std::complex<double> mi( 0., -1. );

 bool (*shear[])( const IntArray&, const std::complex<double>& ) 
    = { sh0, sh1, sh2, sh3, sh4, sh5 };


      /* CAUTION */  // A little test
      /* CAUTION */  if( !(theMapping.IsNilpotent()) ) {
      /* CAUTION */   throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */          "void normalForm( const Mapping& theMapping, /* input */", 
      /* CAUTION */          "This version only supports nilpotent maps." ) );
      /* CAUTION */  }

 bool first_resonance_warning = true;

 // Establishing linear normal form coordinates

 MatrixD  A = theMapping.Jacobian();
 MatrixC  B = A.eigenVectors();

 // Normalizing the linear normal form coordinates

 MatrixD  J( "J", 6 );
 MatrixC  Nx = ( B.transpose() * J * B * J ) * mi;


 double min_diag = -1.0;
 double max_nondiag = -1.0;
 for (int i=0; i<6; ++i) {
     double nxelem = abs(Nx(i,i));
     if ((min_diag < 0.0) || (nxelem < min_diag)) {
         min_diag = nxelem;
     }
 }
 for (int i=0; i<6; ++i) {
     for (int j=0; j<6; ++j) {
         if (i != j) {
             double nxelem = abs(Nx(i,j));
             if ((max_nondiag < 0.0) || (nxelem > max_nondiag)) {
                 max_nondiag = nxelem;
             }
         }
     }
 }

 if ((max_nondiag/min_diag) > MLT1) {
     (*pcerr) << "something's wacko with the matrix" << std::endl;
     (*pcerr) << "maximum nondiag/minimum diag: " << max_nondiag/min_diag << std::endl;
     (*pcerr) << "Nx (should be diagonal)" << std::endl;
     (*pcerr) << Nx << std::endl;
     (*pcerr).flush();
 }

 for( int i = 0; i < 6; ++i) {
   Nx( i, i ) = 1.0 / sqrt( abs( Nx(i,i) ) );  
                                       // "abs" should not be necessary,
                                       // were it not for machine error.
                                       // In principle, could get divide
                                       // by zero here, but it would be bizarre.

   if( abs( ( (std::complex<double>) 1.0 ) - Nx(i,i) ) < 1.0e-10 ) {
     Nx(i,i) = 1.0;
   }

   for (int j = 0; j < 6; ++j) {
     if (i != j) {
       Nx(i,j) = complex_0;
     }
   }
 }

 B = B*Nx;

 // Try to get the phase correct ...

 std::complex<double> m0, cm0, m1, cm1, m2, cm2;
 m0  = B(0,0)/abs(B(0,0));
 cm0 = conj(m0);
 m1  = B(1,1)/abs(B(1,1));
 cm1 = conj(m1);
 m2 = B(2,2)/abs(B(2,2));
 cm2 = conj(m2);

 for( int i=0; i < 6; ++i) {
   B(i,0) *= cm0;
   B(i,3) *= m0;
   B(i,1) *= cm1;
   B(i,4) *= m1;
   B(i,2) *= cm2;
   B(i,5) *= m2;
 }
 if( imag(B(3,0)) > 0.0 ) {
   for( int i=0; i < 6; ++i) {
     m0 = B(i,0);          // NOTE: the variable m0 is reused here and
     B(i,0) = B(i,3);      // below as a dummy variable. This nullifies
     B(i,3) = m0;          // its previous interpretation.
   }
 }
 if( imag(B(4,1)) > 0.0 ) {
   for( int i = 0; i < 6; ++i) {
     m0 = B(i,1);
     B(i,1) = B(i,4);
     B(i,4) = m0;
   }
 }
 if( imag(B(5,2)) > 0.0 ) {
   for( int i = 0; i < 6; ++i) {
     m0 = B(i,2);
     B(i,2) = B(i,5);
     B(i,5) = m0;
   }
 }
 if( imag(B[5][2]) > 0.0 ) {
   for( int i = 0; i < 6; ++i) {
     m0 = B[i][2];
     B[i][2] = B[i][5];
     B[i][5] = m0;
   }
 }

 // Store the result
 *Bptr = B;


 // Some useful matrices

 MatrixC Binv = B.inverse();
 MatrixC D    = Binv * A * B;
 MatrixC Dinv = D.inverse();

 MatrixC lambda(1,D.cols());
 double nu[D.cols()];

 for( int i = 0; i < D.cols(); i++ ) {
   lambda(i) = D(i,i);
   nu[i] = - arg(lambda(i))/M_TWOPI;
 }
      /* CAUTION */  for( int i = 0; i < 6; i++ ) 
      /* CAUTION */  {
      /* CAUTION */   if( fabs( abs(lambda(i)) - 1.0 ) > MLT1 ) {
      /* CAUTION */    ostringstream uic;
      /* CAUTION */    uic  << "Only elliptic fixed points allowed:"
      /* CAUTION */            " |lambda( " << i <<  " )| = " 
      /* CAUTION */         << std::abs(lambda(i))
      /* CAUTION */         << " = 1.0 + ( "
      /* CAUTION */         << ( abs(lambda(i)) - 1.0 )
      /* CAUTION */         << " )";
      /* CAUTION */    throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */           "void normalForm( const Mapping& theMapping, ...)", 
      /* CAUTION */           uic.str().c_str() ) );
      /* CAUTION */   }
      /* CAUTION */    
      /* CAUTION */   if( fabs(lambda(i).imag()) < MLT1 ) {
      /* CAUTION */    ostringstream uic;
      /* CAUTION */    uic << "Eigenvalue " << i << " = " << lambda(i)
      /* CAUTION */        << ": too close to integer or half-integer tune.";
      /* CAUTION */    throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */           "void normalForm( const Mapping& theMapping, ...)", 
      /* CAUTION */           uic.str().c_str() ) );
      /* CAUTION */   }
      /* CAUTION */
      /* CAUTION */   if( fabs(lambda(i).real()) < MLT1 ) {
      /* CAUTION */    ostringstream uic;
      /* CAUTION */    uic << "Eigenvalue " << i << " = " << lambda(i)
      /* CAUTION */        << ": too close to quarter-integer tune.";
      /* CAUTION */    throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */           "void normalForm( const Mapping& theMapping, ...)", 
      /* CAUTION */           uic.str().c_str() ) );
      /* CAUTION */   }
      /* CAUTION */  }
      /* CAUTION */ 
      /* CAUTION */  // A little checking and cleaning.
      /* CAUTION */  for( int i = 0; i < 6; i++ ) {
      /* CAUTION */   if( fabs( abs(D(i,i)) - 1.0 ) > MLT1 ) {
      /* CAUTION */    ostringstream uic;
      /* CAUTION */    uic  << "For now, only elliptic maps allowed: | D( " 
      /* CAUTION */         << i << ", " << i << " ) | = " 
      /* CAUTION */         << std::abs(D(i,i))
      /* CAUTION */         << " = 1.0 + ( "
      /* CAUTION */         << ( abs(D(i,i)) - 1.0 )
      /* CAUTION */         << " )";
      /* CAUTION */    throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */           "void normalForm( const Mapping& theMapping, /* input */", 
      /* CAUTION */           uic.str().c_str() ) );
      /* CAUTION */   }
      /* CAUTION */   for( int j=0; j < 6; ++j) {
      /* CAUTION */    if( j == i ) continue;
      /* CAUTION */    else if( abs( D(i,j) ) > MLT1) {
      /* CAUTION */     throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */            "void normalForm( const Mapping& theMapping, /* input */", 
      /* CAUTION */            "An impossible error has occurred!" ) );
      /* CAUTION */    }
      /* CAUTION */    else D(i,j) = complex_0;
      /* CAUTION */   }
      /* CAUTION */  }
      /* CAUTION */  
      /* CAUTION */  for( int i = 0; i < 6; i++ ) {
      /* CAUTION */   if( fabs( abs(Dinv(i,i)) - 1.0 ) > MLT1 ) {
      /* CAUTION */    ostringstream uic;
      /* CAUTION */    uic  << "For now, only elliptic maps allowed: | Dinv( " 
      /* CAUTION */         << i << ", " << i << " ) | = " 
      /* CAUTION */         << std::abs(Dinv(i,i))
      /* CAUTION */         << " = 1.0 + ( "
      /* CAUTION */         << ( abs(Dinv(i,i)) - 1.0 )
      /* CAUTION */         << " )";
      /* CAUTION */    throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */           "void normalForm( const Mapping& theMapping, /* input */", 
      /* CAUTION */           uic.str().c_str() ) );
      /* CAUTION */   }
      /* CAUTION */   for( int j=0; j < 6; ++j) {
      /* CAUTION */    if( j == i ) continue;
      /* CAUTION */    else if( abs( Dinv(i,j) ) > MLT1) {
      /* CAUTION */     throw( GenericException( __FILE__, __LINE__, 
      /* CAUTION */            "void normalForm( const Mapping& theMapping, /* input */", 
      /* CAUTION */            "Impossible error has occurred!" ) );
      /* CAUTION */    }
      /* CAUTION */    else Dinv(i,j) = complex_0;
      /* CAUTION */   }
      /* CAUTION */  }


 // The original near-identity transformation

 MappingC CL1 = theMapping;

 MappingC id( "ident" );
 MappingC calN = Binv*CL1( B*(Dinv*id) );
 MappingC mapT;

 // And the rest ...

 std::complex<double>  factor, denom, temp;
 int                   ll;
 MappingC              reg;
 MappingC              doc;

 
 for( int k = 0; k <= maxOrder - 2; k++ ) {
  reg = id;
  ll = 0;
  while( ll < k ) reg = N[ll++].expMap( -complex_1, reg );
  reg = calN( reg );
  // -------------
  reg = reg.filter( k+2, k+2 );
  N[k] = reg.filter( shear );

  doc = N[k] - reg;

  for( int i=0; i< 6; ++i ) {

   #if 0
   // Including this line causes an error.
   // lpjm - 2006.08.06
   T[k](i).clear();
   #endif

   for ( JetC::iterator it = doc(i).begin() ; it != doc(i).end(); ++it ) {
    factor = 1.0;

    for( int j=0; j < 6; ++j) {
     temp = complex_1 / lambda(j);
     IntArray exponents =  it->exponents( doc(i).Env() ) ;

     for( int l=0;  l < exponents(j); ++l) {
      factor *= temp;
     }
     // REMOVE: factor *= pow( complex_1 / lambda(j), it->exponents()(j) );
     // REMOVE: factor *= pow( 1.0 / lambda(j), it->exponents()(j) );
    }
    factor *= lambda(i);


    // Either absorption or resonance subtraction ... 
    denom = factor - complex_1;

    if( abs( denom ) <= 1.0e-7 ) 
    {
      N[k](i).addTerm( JLCterm( - it->coefficient(), it->offset_, it->weight_ ) );

      // ?? OBSOLETE ?? N[k](i).addTerm( JLCterm( it->exponents(), - it->coefficient(), CL1.Env() ) );
      
      // If this is not a shear term, print a warning
      // --------------------------------------------
      IntArray exponents =  it->exponents( doc(i).Env() ) ;

      if( ! (shear[i])( exponents, std::complex<double>(0,0) ) )   // second argument is ignored but necessary
      {
        // Print tunes
        // -----------
        if( first_resonance_warning ) {
          first_resonance_warning = false;
          (*pcerr) << "\n-----------------------------" << endl;
          for( int j = 0; j < 6; ++j ) {
            (*pcerr) << "nu_" << j << " = " 
                     << nu[j]
                     << endl;
          }
          for( int j = 0; j < 6; ++j ) {
            (*pcerr) << "lambda(" << j << ") = " 
                     << lambda(j)
                     << " = " 
                     << abs(lambda(j))
                     << "*expi(twopi*("
                     << arg(lambda(j))/M_TWOPI
                     << "))"
                     << endl;
          }
          (*pcerr) << "-----------------------------\n" << endl;
        }

        // Output detailed warning message
        // -------------------------------
        (*pcerr) << __FILE__ << ", Line " << __LINE__ 
                 << ": *** WARNING *** Resonance subtraction omitted for component " 
                 << i 
                 << " with denominator = "
                 << factor
                 << " - 1 = "
                 << denom
                 << endl;

        (*pcerr) << "Exponents = " << exponents
                 << endl;

        int upper_limit = 6/2;    // leave as is to identify as hard-wired for 6
        double defect = 0.0;      // will be fixed in updated versions

        // Print resonance line and defect
        // -------------------------------
        for( int j = 0; j < upper_limit; ++j ) 
        {
          (*pcerr) << '(';

          if( j == i ) {
            (*pcerr) << exponents[j] - exponents[j + upper_limit] - 1;
            defect += double(exponents[j] - exponents[j + upper_limit] - 1)*nu[j];
          }
          else if( j == (i-upper_limit) ) {
            (*pcerr) << exponents[j] - exponents[j + upper_limit] + 1;
            defect += double(exponents[j] - exponents[j + upper_limit] + 1)*nu[j];
          }
          else {
            (*pcerr) << exponents[j] - exponents[j + upper_limit];
            defect += double(exponents[j] - exponents[j + upper_limit])*nu[j];
          }

          (*pcerr) << ")*nu_" << j;

          j == (upper_limit - 1) ? (*pcerr) << " = " : (*pcerr) << " + ";
        }
        (*pcerr) << defect << endl;
      }
    }
    else 
    {
      T[k](i).addTerm( JLCterm( it->coefficient()/denom, it->offset_, it->weight_ ) );

      // ?? OBSOLETE ?? T[k](i).addTerm( JLCterm( it->exponents(), it->coefficient()/denom, CL1.Env() ) );
    }
   }
  }

  // Prepare for the next order

  reg = Dinv*id;
  mapT = T[k].expMap( complex_1, id );
  reg = mapT( reg );
  reg = D*reg;
  reg = calN( reg );
  mapT = T[k].expMap( -complex_1, id );
  calN = mapT( reg );

  // In one line:
  // calN = T[k].expMap( -1.0, calN( D*( T[k].expMap( 1.0, Dinv*id ) ) ) );
 } 

}
