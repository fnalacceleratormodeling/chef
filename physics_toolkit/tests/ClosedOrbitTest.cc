#include <iostream>
#include <complex>

#include <beamline/drift.h>
#include <beamline/sbend.h>
#include <beamline/CF_sbend.h>
#include <beamline/quadrupole.h>
#include <beamline/kick.h>
#include <beamline/beamline.h>
#include <beamline/BmlPtr.h>
#include <beamline/bmlnElmnt.h>
//#include <beamline/ElmPtr.h>
#include <beamline/Particle.h>
#include <beamline/JetParticle.h>
#include <beamline/RefRegVisitor.h>
#include <physics_toolkit/BeamlineContext.h>
#include <physics_toolkit/LattFuncSage.h>
#include <physics_toolkit/ClosedOrbitSage.h>

using namespace std;

int main()
{
    int    const n           = 128;                      //         : number of cells
    double const momentum    = 2.0;                  // [GeV/c] : particle momentum;
    //         :   arbitrary number
    double const bendangle   = M_TWOPI/(2.0*double(n));// [rad]   : dipole bend angle
    double const focus       = 7.0;                    // [m]     : focal length of equivalent
    //         :   thin quad
    double const sepn        = 10.0;                   // [m]     : distance between quad centers
    double const quadlength  = 0.2;                    // [m]     : quadrupole length
    double const pct         = 0.4;                    //         : fraction of space between
    //         :   quads occupied by dipole
    double const bendlength  = pct*(sepn-quadlength);  // [m]     : length of dipole
    double const driftlength = (sepn-quadlength-bendlength)/2;
    double const strength    = 1/(focus*bendlength);   // [m**-2] : quadrupole strength
    //         :   = B'/brho, where
    //         :   brho = momentum/0.299792458

    Proton prtn( PH_NORM_mp );
    prtn.SetReferenceMomentum( momentum );
    double const brho = prtn.ReferenceBRho();
    cout << "Beta = " << prtn.Beta() << endl;
    cout << "brho: " << brho << endl;

    // ------------------
    // Elements
    // ------------------
    drift      o( "o", driftlength );
    quadrupole f( "f", quadlength, strength*brho);
    quadrupole d( "d", quadlength, -strength*brho );
    sbend      b( "b", bendlength, brho*bendangle/bendlength, bendangle);
    hkick        hk("hk", 0.02);

    cout << "driftlength: " << driftlength << endl;
    cout << "quadlength: " << quadlength << endl;
    cout << "bendlength: " << bendlength << endl;
    cout << "quadrupole strength(1/(focus*length): " << strength << endl;

    // ------------------
    // Lattices
    // ------------------
    BmlPtr fobodoboPtr( new beamline );
    fobodoboPtr->append(ElmPtr(f.Clone()));
    fobodoboPtr->append(ElmPtr(o.Clone()));
    fobodoboPtr->append(ElmPtr(b.Clone()));
    fobodoboPtr->append(ElmPtr(o.Clone()));
    fobodoboPtr->append(ElmPtr(d.Clone()));
    fobodoboPtr->append(ElmPtr(o.Clone()));
    fobodoboPtr->append(ElmPtr(b.Clone()));
    fobodoboPtr->append(ElmPtr(o.Clone()));

    BmlPtr fobodobo128Ptr( new beamline );
    // beamline model;
    // model.append( 128*fobodobo );
    // put kick at front
    fobodobo128Ptr->append(hk);
    for( int i = 0; i < n; ++i ) {
        fobodobo128Ptr->append( BmlPtr(fobodoboPtr->Clone()) );
    }
    fobodobo128Ptr->setEnergy( prtn.ReferenceEnergy() );

    ClosedOrbitSage cosage( fobodobo128Ptr );
    
    Proton p(prtn);
    cosage.findClosedOrbit(p);
    std::cout.precision(15);
    
    std::cout << "Finding Closed Orbit with particle solver returns: " << p.State() << std::endl;
    
    std::cout << "Check propagation of closed orbit from particle solver state" << std::endl;
    Proton p2(p);
    
    std::cout << "Start: " << p2.State() << std::endl;
    fobodobo128Ptr->propagate(p2);
    std::cout << "End: " << p2.State() << std::endl;
    std::cout << "Difference: " << p2.get_x()-p.get_x() <<" , " << p2.get_y()-p.get_y() << " , " << p2.get_npx()-p.get_npx() << " , " << p2.get_npy()-p.get_npy() << std::endl;

    JetProton::createStandardEnvironments(1);
    
    JetProton jpr3(prtn);
    ClosedOrbitSage cosage2( BmlPtr(fobodobo128Ptr->Clone()) );
    cosage2.findClosedOrbit(jpr3);
    Proton pr3(jpr3);
    std::cout << "Finding Closed Orbit with jet solver returns: " << pr3.State() << std::endl;
    std::cout << "Check propagation of closed orbit from Jet solver state" << std::endl;
    Proton pr3a(pr3);
    std::cout << "Start: " << pr3a.State() << std::endl;
    fobodobo128Ptr->propagate(pr3a);
    std::cout << "End: " << pr3a.State() << std::endl;
    std::cout << "Difference: " << pr3a.get_x()-pr3.get_x() << " , " << pr3a.get_y()-pr3.get_y() << " , " << pr3a.get_npx()-pr3.get_npx() << " , " << pr3a.get_npy()-pr3.get_npy() << std::endl;
    return 0;
}
