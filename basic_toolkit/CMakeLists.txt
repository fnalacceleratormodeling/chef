file(GLOB HEADERS "${CHEF_SOURCE_DIR}/basic_toolkit/${INCLUDE_INSTALL_DIR}/*.h")
file(GLOB TEMPLATES "${CHEF_SOURCE_DIR}/basic_toolkit/${INCLUDE_INSTALL_DIR}/*.tcc")

install(FILES ${HEADERS} ${TEMPLATES} DESTINATION ${INCLUDE_INSTALL_DIR}/basic_toolkit)

add_subdirectory(src)
add_subdirectory(tests)
