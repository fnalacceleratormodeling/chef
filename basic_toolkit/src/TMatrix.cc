/*************************************************************************
**************************************************************************
**************************************************************************
******
******  Basic TOOLKIT:  Low level utility C++ classes.
******
******  File:      TMatrix.cc
******
******  Copyright (c) Universities Research Association, Inc.
******                All Rights Reserved
******
******  Usage, modification, and redistribution are subject to terms          
******  of the License supplied with this software.
******  
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S. and Foreign Copyright Laws. 
******
******                                                                
******  Author:    Leo Michelotti                                     
******             Fermilab                                           
******             P.O.Box 500                                        
******             Mail Stop 220                                      
******             Batavia, IL   60510                                
******             Email: michelotti@fnal.gov                         
****** 
******  Revision (Sep 2005):
******
******             Jean-Francois Ostiguy
******             ostiguy@fnal.gov                                   
******             
******   - reorganized code to support explicit template instantiations
******   - eliminated separate MatrixC class implementation
****** 
**************************************************************************
*************************************************************************/



#include <iostream>
#include <iomanip>
#include <sstream>

#include <basic_toolkit/TML.h>
#include <basic_toolkit/MLPtr.h>
#include <basic_toolkit/TMatrix.h>
#include <basic_toolkit/VectorD.h>
#include <basic_toolkit/MathConstants.h>
#include <basic_toolkit/PhysicsConstants.h>
#include <basic_toolkit/iosetup.h>
#include <basic_toolkit/utils.h>


#ifdef WIN32
#include <Distribution.h> // for drand48
#endif


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

template<>
template<>
TMatrix<std::complex<double> >::TMatrix( TMatrix<double> const& m)
{ 
  ml_ = m.ml_; // implicit conversion
  return; 
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

template<>
TMatrix<double> TMatrix<double>::dagger() const {

//  for a real matrix, the Hermitian conjugate and the transpose
//  are the same
    
  TMatrix<double> ret;
  ret.ml_ = ml_->transpose();
  return ret;
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

template<>
TMatrix<std::complex<double> > TMatrix<std::complex<double> >::dagger() const
{
  TMatrix<std::complex<double> > ret;
  ret.ml_= ml_->dagger();
  return ret;
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

TMatrix<double> real( TMatrix<std::complex<double> > const& x )
{
  TMatrix<double> ret; 
  ret.ml_= real_part(x.ml_); 
  return ret; 
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

TMatrix<double> imag( TMatrix<std::complex<double> > const& x ) 
{
  TMatrix<double> ret; 
  ret.ml_= imag_part(x.ml_); 
  return ret; 
}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

template<>
void TMatrix<double>::SVD( TMatrix<double>& U, Vector& W, TMatrix<double>& V) const
{
  ml_->SVD( U.ml_, W, V.ml_);

}
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

TMatrix<std::complex<double>  > operator*( TMatrix<std::complex<double>  > const& x, TMatrix<double> const& y)
{
 
  TMatrix<std::complex<double> > z = y; //implicit conversion 
  TMatrix<std::complex<double> > ret;

  ret.ml_ = multiply<std::complex<double> >(x.ml_, z.ml_); 

  return ret; 
}


//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

TMatrix<std::complex<double> > operator*(TMatrix<double> const& x, TMatrix<std::complex<double> > const& y)
{

  TMatrix<std::complex<double> > z = x; //implicit conversion 

  TMatrix<std::complex<double> > ret;
  ret.ml_ = multiply<std::complex<double> >(z.ml_, y.ml_); 
  return ret; 

}

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/*** static function ***/

template<>
Vector 
TMatrix<double>::backSubstitute(TMatrix<double> const& U, Vector const& W, TMatrix<double> const& V, Vector const& rhs, double threshold) { 

  return TML<double>::backSubstitute(U.ml_, W, V.ml_, rhs, threshold);
 
}


