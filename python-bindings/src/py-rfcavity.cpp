/*******************************************************************************
********************************************************************************
********************************************************************************
******
******  Python bindings for mxyzpltk/beamline libraries 
******  
******                                    
******  File:      py-rfcavity.cpp
******                                                                
******  Copyright (c) Universities Research Association, Inc./ Fermilab    
******                All Rights Reserved                             
******
******  Software and documentation created under 
******  U.S. Department of Energy Contract No. DE-AC02-76CH03000. 
******  The U.S. Government retains a world-wide non-exclusive, 
******  royalty-free license to publish or reproduce documentation 
******  and software for U.S. Government purposes. This software 
******  is protected under the U.S.and Foreign Copyright Laws. 
******                                                                
******  Author:    Jean-Francois Ostiguy                                     
******                                                                
******             Fermi National Laboratory, Batavia, IL   60510                                
******             ostiguy@fnal.gov                         
******
********************************************************************************
********************************************************************************
*******************************************************************************/

#include <boost/python.hpp>
#include <beamline/rfcavity.h>

using namespace boost::python;

// wrappers for overloaded definitions
void (rfcavity::*rfcavity_setHarmonicNumber_int)(int n) = &rfcavity::setHarmonicNumber;
void (rfcavity::*rfcavity_setHarmonicNumber_double)(double x) = &rfcavity::setHarmonicNumber;

void (thinrfcavity::*thinrfcavity_setHarmonicNumber_int)(int n) = &thinrfcavity::setHarmonicNumber;
void (thinrfcavity::*thinrfcavity_setHarmonicNumber_double)(double) = &thinrfcavity::setHarmonicNumber;
void wrap_rfcavity () {
  
class_<rfcavity, bases<bmlnElmnt>, RFCavityPtr >("rfcavity")
  .def( init<const char*>() )
  .def( init<char*, double,double,double,double,double,double>() )    
  .def("getHarmonicNumber", &rfcavity::getHarmonicNumber)
  .def("getPhi",             &rfcavity::getPhi          )
  .def("setHarmonicNumber", rfcavity_setHarmonicNumber_int)
  .def("setHarmonicNumber", rfcavity_setHarmonicNumber_double)
  .def("setPhi",             &rfcavity::setPhi)
  .def("setRadialFrequency",    &rfcavity::setRadialFrequency)
  .def("setDisplacedFrequency", &rfcavity::setDisplacedFrequency)
  .def("setFrequency",             &rfcavity::setFrequency)
  .def("getRadialFrequency", &rfcavity::getRadialFrequency)
  .def("getQ",               &rfcavity::getQ)
  .def("getR",               &rfcavity::getR)
  .def("addHarmonic", &rfcavity::addHarmonic)
  .def("turnUpdate", &rfcavity::turnUpdate)
  .def("getCumulativeDisplacedPhaseSlip", &rfcavity::getCumulativeDisplacedPhaseSlip)
  .def("setCumulativeDisplacedPhaseSlip", &rfcavity::setCumulativeDisplacedPhaseSlip)
  .def("setStrength",             &rfcavity::setStrength)
  .def("Type",               &rfcavity::Type);


class_<thinrfcavity, bases<bmlnElmnt>, ThinRFCavityPtr >("thinrfcavity", init<char *>() )
  .def(init< char*,    double,   double, double, double, double> () )    
  .def("getHarmonicNumber", &thinrfcavity::getHarmonicNumber)
  .def("getPhi",             &thinrfcavity::getPhi          )
  .def("setHarmonicNumber", thinrfcavity_setHarmonicNumber_int)
  .def("setHarmonicNumber", thinrfcavity_setHarmonicNumber_double)
  .def("setPhi",             &thinrfcavity::setPhi)
  .def("setRadialFrequency",             &thinrfcavity::setRadialFrequency)
  .def("setDisplacedFrequency", &thinrfcavity::setDisplacedFrequency)
  .def("setFrequency",             &thinrfcavity::setFrequency)
  .def("getRadialFrequency", &thinrfcavity::getRadialFrequency)
  .def("getQ",               &thinrfcavity::getQ)
  .def("getR",               &thinrfcavity::getR)
  .def("addHarmonic", &thinrfcavity::addHarmonic)
  .def("turnUpdate", &thinrfcavity::turnUpdate)
  .def("getCumulativeDisplacedPhaseSlip", &thinrfcavity::getCumulativeDisplacedPhaseSlip)
  .def("setCumulativeDisplacedPhaseSlip", &thinrfcavity::setCumulativeDisplacedPhaseSlip)
  .def("Type",               &thinrfcavity::Type);

}


